package com.example.demo.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.WindowManager;

import com.example.demo.MyApplication;

/**
 * view 工具类
 * Created by xtxiaolu on 2023/9/1
 */
public class ViewUtil {

    private ViewUtil() {

    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources()
                                   .getDisplayMetrics().density;
        return (int)(dpValue * scale + 0.5f);
    }

    public static int dip2px(float dpValue) {
        Context context = MyApplication.getInstance();
        final float scale = context.getResources()
                                   .getDisplayMetrics().density;
        return (int)(dpValue * scale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     *
     * @param spValue
     * @param （DisplayMetrics类中属性scaledDensity）
     * @return
     */
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources()
                                       .getDisplayMetrics().scaledDensity;
        return (int)(spValue * fontScale + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param pxValue （DisplayMetrics类中属性scaledDensity）
     * @return
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources()
                                       .getDisplayMetrics().scaledDensity;
        return (int)(pxValue / fontScale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources()
                                   .getDisplayMetrics().density;
        return (int)(pxValue / scale + 0.5f);
    }

    /**
     * 获取屏幕宽度
     *
     * @return
     */
    public static int getScreenWidth() {
        Context context = MyApplication.getInstance();
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Point point = new Point();
        wm.getDefaultDisplay()
          .getSize(point);
        return point.x;
    }

    /**
     * 获取屏幕高度
     *
     * @return
     */
    public static int getScreenHeight() {
        Context context = MyApplication.getInstance();
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Point point = new Point();
        wm.getDefaultDisplay()
          .getSize(point);
        return point.y;
    }

    public static float getRawSize(Context context, int unit, float size) {
        Resources r;

        if (context == null) {
            r = Resources.getSystem();
        } else {
            r = context.getResources();
        }

        return TypedValue.applyDimension(unit, size, r.getDisplayMetrics());
    }
}
