package com.example.demo.util;


public class TimeRecordUtil {


    private long startTime;

    public static TimeRecordUtil instance;

    public static TimeRecordUtil getInstance() {
        if (instance == null) {
            synchronized (TimeRecordUtil.class) {
                if (instance == null) {
                    instance = new TimeRecordUtil();
                }
            }
        }
        return instance;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * 判断指定时间是否超过指定的时间间隔
     *
     * @param currentTime       要判断的时间戳
     * @param timeIntervalMillis 时间间隔，单位为毫秒
     * @return 如果指定时间距离上次时间超过指定时间间隔返回true，否则返回false
     * 在示例中，600_000 表示 600000，也就是 600 秒，或者 10 分钟（1 分钟 = 60 秒）。
     * 使用下划线来分隔数字，使得 600000 更容易被人们理解为 10 分钟的时间间隔。在编译时，下划线将被忽略，数字值为 600000。
     */
    public boolean isExceedTimeInterval(long currentTime, long timeIntervalMillis) {
        // 判断是否超过指定的时间间隔
        if (startTime == 0) {
            return false;
        }
        return currentTime >= startTime && (currentTime - startTime) >= timeIntervalMillis;
    }

    // 清空 startTime
    public void clearStartTime() {
        startTime = 0;
    }
}
