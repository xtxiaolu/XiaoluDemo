package com.example.demo.util;

import com.example.demo.model.UserBean;

import java.util.Comparator;

public class PinyinComparator implements Comparator<UserBean> {

	public int compare(UserBean o1, UserBean o2) {
		String letters1 = o1.getLetters();
		String letters2 = o2.getLetters();

		// 检查是否为 null
		if (letters1 == null && letters2 == null) {
			return 0; // 两者都为 null，可以认为相等
		} else if (letters1 == null) {
			return -1; // o1 的 letters 为 null，o2 大于 o1
		} else if (letters2 == null) {
			return 1; // o2 的 letters 为 null，o1 大于 o2
		} else if (letters1.equals("@") || letters2.equals("#")) {
			return 1;
		} else if (letters1.equals("#") || letters2.equals("@")) {
			return -1;
		} else {
			return letters1.compareTo(letters2);
		}
	}
}