package com.example.demo;

import com.chad.library.BuildConfig;
import com.example.library.MApplication;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

/**
 * @author: xtxiaolu
 * @date: 2023/6/9
 * 描述:
 */
public class MyApplication extends MApplication {
    private static MyApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initLog();
    }

    private void initLog() {
        /**
         * showThreadInfo   （可选）是否显示线程信息。 默认值为true
         * methodCount(3)   （可选）要显示的方法行数。 默认2
         * methodOffset(0)  （可选）设置调用堆栈的函数偏移值，0的话则从打印该Log的函数开始输出堆栈信息，默认是0
         * tag("Logger")    （可选）TAG内容. 默认是 PRETTY_LOGGER
         */
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(com.orhanobut.logger.BuildConfig.DEBUG)
                .methodCount(3)
                .methodOffset(0)
                .build();

        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
        Logger.addLogAdapter(new DiskLogAdapter());

    }


    public static MyApplication getInstance() {
        return instance;
    }
}
