package com.example.demo.ui.home;

import android.graphics.Color;
import android.icu.math.BigDecimal;
import android.os.Bundle;
import android.widget.CompoundButton;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityCounterViewBinding;
import com.example.demo.view.CounterView;
import com.example.demo.view.CustomCounterView;
import com.example.demo.view.InputCounterView;
import com.orhanobut.logger.Logger;

/**
 * @author: xtxiaolu
 * @date: 2023/8/17
 * 描述: 加减自定义控件
 */
public class CounterViewActivity extends BaseActivity {
    private ActivityCounterViewBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCounterViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // 可以设置其他属性，如最小值、最大值、步长和默认值
        binding.inputCounterView.setMinValue(0);
        binding.inputCounterView.setMaxValue(100);
        binding.inputCounterView.setIncrement(1);
        binding.inputCounterView.setDefaultValue(0);

        // 设置CounterView的监听器
        binding.inputCounterView.setOnValueChangedListener(new InputCounterView.OnValueChangedListener() {
            @Override
            public void onValueChanged(int newValue) {
                // 当值发生变化时执行的操作
                // 这里可以根据新的值执行你的逻辑
                Logger.d("CounterViewActivity InputCounterView onValueChanged 数据变化 " + newValue);
            }
        });

        // 设置最小值、最大值、步进值和默认值
        binding.customCounter.setValueTextColor(Color.BLACK);
        binding.customCounter.setValueTextSize(18);
        binding.customCounter.setMinValue(0);
        binding.customCounter.setMaxValue(100);
        binding.customCounter.setIncrement(1);
        binding.customCounter.setValue(50);

        // 设置数值变化监听器
        binding.customCounter.setOnValueChangedListener(new CustomCounterView.OnValueChangedListener() {
            @Override
            public void onValueChanged(int newValue) {
                // 在这里处理数值变化事件
                Logger.d("CounterViewActivity CustomCounterView onValueChanged 数据变化 " + newValue);
            }
        });

        binding.counterView.setMinValue(BigDecimal.ZERO);
        binding.counterView.setMaxValue(BigDecimal.TEN);
        binding.counterView.setIncrement(BigDecimal.ONE);
        binding.counterView.setDefaultValue(BigDecimal.ZERO);
        binding.counterView.setDecimalEnabled(false,2);

        binding.counterView.setOnValueChangedListener(new CounterView.OnValueChangedListener() {
            @Override
            public void onValueChanged(BigDecimal newValue) {
                // 在这里处理数值变化事件
                Logger.d("CounterViewActivity CounterView onValueChanged 数据变化 " + newValue.toString());
            }
        });

        binding.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Logger.d("Switch Switch state changed: " + isChecked);
                if (isChecked) {// 开关被选中
                    Logger.d("Switch Enabling decimal input");
                    binding.counterView.setDecimalEnabled(true,2);
                } else {
                    Logger.d("Switch Disabling decimal input");
                    binding.counterView.setDecimalEnabled(false,2);
                }
            }
        });
    }
}