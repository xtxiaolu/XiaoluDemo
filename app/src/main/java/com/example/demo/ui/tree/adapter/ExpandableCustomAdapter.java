package com.example.demo.ui.tree.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demo.R;
import com.example.demo.model.DepartmentBean;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/11
 * 描述:嵌套的ExpandableListView实现第二层和第三层
 */
public class ExpandableCustomAdapter extends BaseExpandableListAdapter {
    private List<DepartmentBean> departments;

    public ExpandableCustomAdapter(List<DepartmentBean> departments) {
        this.departments = departments;
    }

    @Override
    public int getGroupCount() {
        return departments.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        DepartmentBean department = departments.get(groupPosition);
        List<DepartmentBean> children = department.getChildren();
        return children != null ? children.size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return departments.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        DepartmentBean department = departments.get(groupPosition);
        List<DepartmentBean> children = department.getChildren();
        return children != null ? children.get(childPosition) : null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_group_item_layout, parent, false);
        }

        TextView groupTextView = convertView.findViewById(R.id.childGroupTextView);
        DepartmentBean department = (DepartmentBean) getGroup(groupPosition);
        groupTextView.setText(department.getDepartName());

        ImageView arrowImageView = convertView.findViewById(R.id.arrowImageView);
        if (getChildrenCount(groupPosition) > 0) {
            arrowImageView.setVisibility(View.VISIBLE);
            if (isExpanded) {
                arrowImageView.setImageResource(R.drawable.arrow_down);
            } else {
                arrowImageView.setImageResource(R.drawable.arrow_right);
            }
        } else {
            arrowImageView.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.leaf_child_item_layout, parent, false);
        }

        TextView childTextView = convertView.findViewById(R.id.leafChildTextView);
        DepartmentBean department = (DepartmentBean) getChild(groupPosition, childPosition);
        childTextView.setText(department.getDepartName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
