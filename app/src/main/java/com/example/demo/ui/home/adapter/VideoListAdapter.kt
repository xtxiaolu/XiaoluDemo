package com.example.demo.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.databinding.ItemVideoBinding
import com.example.demo.model.ChannelBean
import com.example.demo.model.VideoBean

class VideoListAdapter(
    private val videos: List<VideoBean>,
    private val channelItemClickListener: OnChannelItemClickListener,
    private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<VideoListAdapter.VideoViewHolder>(), OnChannelItemClickListener {

    inner class VideoViewHolder(private val binding: ItemVideoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var isExpanded: Boolean = false

        init {
//            binding.root.setOnClickListener {
//                isExpanded = !isExpanded
//                binding.arrowIcon.rotation = if (isExpanded) 90f else 0f
//                itemClickListener.onItemClicked(videos[adapterPosition], isExpanded)
//                notifyDataSetChanged() // 刷新列表，显示变更
//            }
        }

        fun bind(video: VideoBean) {
            binding.videoName.text = video.name
            binding.videoIntroduce.text = video.introduce

            // 添加空检查
            val channelList = video.channels ?: emptyList()

            val channelAdapter = VideoChannelAdapter(channelList, this@VideoListAdapter)
            binding.channelRecyclerView.layoutManager = LinearLayoutManager(binding.root.context)
            binding.channelRecyclerView.adapter = channelAdapter

            // 根据展开状态设置 channelRecyclerView 的可见性
            binding.channelRecyclerView.visibility = if (isExpanded) View.VISIBLE else View.GONE

            // 手动设置箭头的旋转角度
            binding.arrowIcon.rotation = if (isExpanded && video.channels != null) 90f else 0f

            // 在 bind 方法中注册点击事件
            binding.root.setOnClickListener {
                isExpanded = !isExpanded
                binding.arrowIcon.rotation = if (isExpanded && video.channels != null) 90f else 0f
                itemClickListener.onItemClicked(video, isExpanded)
                notifyDataSetChanged() // 刷新列表，显示变更
            }

//            val channelAdapter = VideoChannelAdapter(video.channels, this@VideoListAdapter)
//            binding.channelRecyclerView.layoutManager = LinearLayoutManager(binding.root.context)
//            binding.channelRecyclerView.adapter = channelAdapter
//
//            // 根据展开状态设置 channelRecyclerView 的可见性
//            binding.channelRecyclerView.visibility = if (isExpanded) View.VISIBLE else View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val binding = ItemVideoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VideoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(videos[position])
    }

    override fun getItemCount(): Int = videos.size

    override fun onChannelItemClicked(channel: ChannelBean) {
        channelItemClickListener.onChannelItemClicked(channel)
    }
}