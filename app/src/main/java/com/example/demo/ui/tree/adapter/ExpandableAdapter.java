package com.example.demo.ui.tree.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demo.R;
import com.example.demo.model.DepartmentBean;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/10
 * 描述:ExpandableListView 实现第一层 并嵌套第二层ExpandableListView
 * 用嵌套来实现ExpandableListView三层列表
 * ExpandableListView默认支持两层列表
 * 注意：嵌套的ExpandableListView需要重写否则嵌套冲突导致数据显示不全
 * 重写ExpandableListView：CustomExpandableListView.java
 */
public class ExpandableAdapter extends BaseExpandableListAdapter {
    private List<DepartmentBean> departments;

    public ExpandableAdapter(List<DepartmentBean> departments) {
        this.departments = departments;
    }

    @Override
    public int getGroupCount() {
        return departments.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return departments.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return departments.get(groupPosition).getChildren();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_item_layout, parent, false);
        }

        TextView groupTextView = convertView.findViewById(R.id.groupTextView);
        DepartmentBean department = (DepartmentBean) getGroup(groupPosition);
        groupTextView.setText(department.getDepartName());

        ImageView arrowImageView = convertView.findViewById(R.id.arrowImageView);
        if (getChildrenCount(groupPosition) > 0) {
            arrowImageView.setVisibility(View.VISIBLE);
            if (isExpanded) {
                arrowImageView.setImageResource(R.drawable.arrow_down);
            } else {
                arrowImageView.setImageResource(R.drawable.arrow_right);
            }
        } else {
            arrowImageView.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_item_layout, parent, false);
        }

        ExpandableListView subExpandableListView = convertView.findViewById(R.id.subExpandableListView);
        List<DepartmentBean> children = (List<DepartmentBean>) getChild(groupPosition, childPosition);
        ExpandableCustomAdapter subAdapter = new ExpandableCustomAdapter(children);
        subExpandableListView.setAdapter(subAdapter);

        subExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int i) {
                DepartmentBean departmentBean = children.get(i);
                if (itemClickListener!=null){
                    itemClickListener.onGroupItemClick(i,departmentBean);
                }
            }
        });

        subExpandableListView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {
            DepartmentBean departmentBean = children.get(i).getChildren().get(i1);
            if (itemClickListener!=null){
                itemClickListener.onGroupItemClick(i,departmentBean);
            }
            return true;
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private OnItemClickListener itemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onGroupItemClick(int groupPosition, Object o);
        void onChildItemClick(int groupPosition, int childPosition, Object o);
    }

}
