package com.example.demo.ui.tree;

import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityTreeBinding;
import com.example.demo.model.BaseBean;
import com.example.demo.model.TreeItemBean;
import com.example.demo.ui.tree.adapter.TreeAdapter;
import com.example.demo.util.MyJson;
import com.example.demo.util.ToastUtil;
import com.example.library.utils.AssetsUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author: xtxiaolu
 * @date: 2023/11/29
 * 描述: 多级树 不固定层级
 */
public class TreeActivity extends BaseActivity {
    private ActivityTreeBinding binding;
    private List<TreeItemBean> treeItemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTreeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String rqsJson = AssetsUtil.getAssetsJson(this,"MultiLevelTree.json");
        BaseBean data = MyJson.fromJson(rqsJson, BaseBean.class);
        treeItemList = MyJson.fromJson(MyJson.toJson(data.getData()), new TypeToken<List<TreeItemBean>>() {
        }.getType());

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        TreeAdapter treeAdapter = new TreeAdapter(this, treeItemList);
        binding.recyclerView.setAdapter(treeAdapter);

        treeAdapter.setOnItemClickListener((view, position, model) -> {
            ToastUtil.showShort("当前点击的数据为 " + model.getTypeName());
        });
    }
}