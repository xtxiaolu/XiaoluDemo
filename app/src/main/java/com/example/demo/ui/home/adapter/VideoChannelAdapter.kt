package com.example.demo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.databinding.ItemChannelBinding
import com.example.demo.model.ChannelBean

class VideoChannelAdapter(
    private val channels: List<ChannelBean>,
    private val listener: OnChannelItemClickListener
) : RecyclerView.Adapter<VideoChannelAdapter.ChannelViewHolder>() {

    inner class ChannelViewHolder(private val binding: ItemChannelBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.isClickable = true
        }
        fun bind(channel: ChannelBean) {
            binding.channelName.text = channel.name
            binding.channelIntroduce.text = channel.introduce

            binding.root.setOnClickListener {
                listener.onChannelItemClicked(channel)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val binding = ItemChannelBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChannelViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.bind(channels[position])
    }

    override fun getItemCount(): Int = channels.size
}