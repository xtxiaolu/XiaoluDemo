package com.example.demo.ui.home.adapter

import com.example.demo.model.ChannelBean

interface OnChannelItemClickListener {
    fun onChannelItemClicked(channel: ChannelBean)
}