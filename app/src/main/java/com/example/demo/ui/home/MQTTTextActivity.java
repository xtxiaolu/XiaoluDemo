package com.example.demo.ui.home;

import android.os.Bundle;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityMqqtTextBinding;
import com.example.demo.util.MQTTClient;

/**
 * @author: xtxiaolu
 * @date: 2023/8/17
 * 描述: MQTT 测试类
 */
public class MQTTTextActivity extends BaseActivity {
    private ActivityMqqtTextBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMqqtTextBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());



        MQTTClient mqttClient = new MQTTClient(this,
                "tcp://120.133.52.104:1883", "AndroidClient");
        mqttClient.connect();
        mqttClient.subscribe("topic/example");
        mqttClient.publish("topic/example", "Hello, MQTT!");


    }
}
