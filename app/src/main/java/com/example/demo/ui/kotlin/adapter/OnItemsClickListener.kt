package com.example.demo.ui.kotlin.adapter

interface OnItemsClickListener {
    fun onHeaderClick(position: Int)
    fun onItemButtonClick(position: Int)
}