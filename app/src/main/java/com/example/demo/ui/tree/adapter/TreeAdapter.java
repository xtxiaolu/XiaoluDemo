package com.example.demo.ui.tree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo.R;
import com.example.demo.databinding.ItemLevelTreeBinding;
import com.example.demo.model.TreeItemBean;

import java.util.List;

public class TreeAdapter extends RecyclerView.Adapter<TreeAdapter.TreeViewHolder> {

    private final Context mContext;
    private final List<TreeItemBean> treeItemList;
    private int selectPosition = RecyclerView.NO_POSITION;

    public TreeAdapter(Context context, List<TreeItemBean> list) {
        this.mContext = context;
        this.treeItemList = list;
    }

    public void setSelectPosition(int position) {
        this.selectPosition = position;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TreeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLevelTreeBinding binding = ItemLevelTreeBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new TreeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TreeViewHolder holder, int position) {
        TreeItemBean treeItem = treeItemList.get(position);
        holder.bind(treeItem);
    }

    @Override
    public int getItemCount() {
        return treeItemList.size();
    }

    public class TreeViewHolder extends RecyclerView.ViewHolder {

        private final ItemLevelTreeBinding binding;

        public TreeViewHolder(ItemLevelTreeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(TreeItemBean treeItem) {
            // 根据层级设置左边距
            int leftPadding = treeItem.getLevel() * 50;
            binding.tvName.setPadding(leftPadding, 0, 0, 0);
            binding.tvName.setText(treeItem.getTypeName());

            // 根据展开状态设置箭头图标
            if (treeItem.getChildren() != null && !treeItem.getChildren().isEmpty()) {
                if (treeItem.isOpen()) {
                    binding.ivNext.setRotation(90);
                } else {
                    binding.ivNext.setRotation(0);
                }
            } else {
                binding.ivNext.setVisibility(View.GONE);
            }

            // 设置点击事件
            itemView.setOnClickListener(v -> {
                // 切换展开状态
                treeItem.setOpen(!treeItem.isOpen());
                // 局部刷新，只刷新当前点击的位置和其子项的数据
                notifyItemChanged(getBindingAdapterPosition());
                if (onItemClickListener != null){
                    onItemClickListener.onItemClick(v,getBindingAdapterPosition(),treeItem);
                }
                setSelectPosition(getBindingAdapterPosition());
            });

            // 如果没有数据子列表不显示
            binding.recyclerView.setVisibility(treeItem.isOpen() ? View.VISIBLE : View.GONE);

            // 如果有子数据，设置适配器并显示箭头
            if (treeItem.getChildren() != null && !treeItem.getChildren().isEmpty()) {
                binding.ivNext.setVisibility(View.VISIBLE);
                binding.recyclerView.setVisibility(treeItem.isOpen() ? View.VISIBLE : View.GONE);
                // 计算子项的级别，加上适当的偏移量
                int childLevel = treeItem.getLevel() + 1;
                for (TreeItemBean child : treeItem.getChildren()) {
                    child.setLevel(childLevel);
                }
                binding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                TreeAdapter childAdapter = new TreeAdapter(mContext, treeItem.getChildren());
                binding.recyclerView.setAdapter(childAdapter);

                childAdapter.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, TreeItemBean model) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(view, position, model);
                        }
                    }
                });
            } else {    // 没有子数据，隐藏箭头
                binding.ivNext.setVisibility(View.GONE);
                binding.recyclerView.setVisibility(View.GONE);
            }

            // 根据是否带有右箭头来设置背景颜色
            itemView.setBackgroundColor(ContextCompat.getColor(mContext, binding.ivNext.getVisibility() == View.VISIBLE
                    && getBindingAdapterPosition() == selectPosition ? R.color.bg_default : R.color.white));
        }
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, TreeItemBean model);
    }
}