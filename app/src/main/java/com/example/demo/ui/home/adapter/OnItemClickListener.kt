package com.example.demo.ui.home.adapter

import com.example.demo.model.VideoBean

interface OnItemClickListener {
    fun onItemClicked(video: VideoBean, isExpanded: Boolean)
}