package com.example.demo.ui.home.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.demo.R;
import com.example.demo.model.UserBean;

import java.util.List;

public class UserAdapter extends BaseQuickAdapter<UserBean, BaseViewHolder> {
    private List<UserBean> userList;

    public UserAdapter(@Nullable List<UserBean> data) {
        super(R.layout.item_user, data);
        this.userList = data;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, UserBean item) {
        helper.setText(R.id.tv_contacts_name, item.getRealName());
        helper.setText(R.id.tv_department, item.getOrganizationName());
        helper.setText(R.id.tv_phone, item.getUserPhone());


        helper.addOnClickListener(R.id.llUserName);
        helper.addOnClickListener(R.id.ivPhone);
    }


    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = userList.get(i).getLetters();
            if (sortStr != null && !sortStr.isEmpty()) {
                char firstChar = sortStr.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }
        }
        return -1;
    }
}
