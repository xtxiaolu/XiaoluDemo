package com.example.demo.ui.home;

import static android.Manifest.permission.NEARBY_WIFI_DEVICES;

import android.net.wifi.ScanResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityWifiDemoBinding;
import com.example.demo.util.ToastUtil;
import com.example.demo.util.WifiUtils;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallbackWithBeforeParam;
import com.permissionx.guolindev.callback.ForwardToSettingsCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.permissionx.guolindev.request.ForwardScope;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/12
 * 描述: Wi-Fi 测试工具类
 */
public class WifiDemoActivity extends BaseActivity {
    private ActivityWifiDemoBinding binding;
    private WifiUtils wifiUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWifiDemoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        if (Build.VERSION.SDK_INT >= 33) {
            PermissionX.init(this)
                    .permissions(NEARBY_WIFI_DEVICES)
                    .onExplainRequestReason(new ExplainReasonCallbackWithBeforeParam() {
                        @Override
                        public void onExplainReason(ExplainScope scope, List<String> deniedList, boolean beforeRequest) {
                            scope.showRequestReasonDialog(deniedList, "即将申请的权限是程序必须依赖的权限", "我已明白");
                        }
                    }).onForwardToSettings(new ForwardToSettingsCallback() {
                        @Override
                        public void onForwardToSettings(ForwardScope scope, List<String> deniedList) {
                            scope.showForwardToSettingsDialog(deniedList, "您需要去应用程序设置当中手动开启权限", "我已明白");
                        }
                    }).request(new RequestCallback() {
                        @Override
                        public void onResult(boolean allGranted, List<String> grantedList, List<String> deniedList) {
                            if (allGranted) {
                                initWifi();
                            } else {
                                ToastUtil.showShort("您拒绝了如下权限：" + deniedList + "有些功能您将无法正常使用,如需使用请手动开启权限!");
                                finish();
                            }
                        }
                    });
        }
    }

    private void initWifi() {
        // 初始化Wi-Fi工具类
        wifiUtils = WifiUtils.getInstance(this);

        binding.openWifiBtn.setOnClickListener(v -> {
            wifiUtils.openWifi();
            ToastUtil.showShort("Wi-Fi已打开");
        });

        binding.closeWifiBtn.setOnClickListener(v -> {
            wifiUtils.closeWifi();
            ToastUtil.showShort("Wi-Fi已关闭");
        });

        binding.connectWifiBtn.setOnClickListener(v -> {
            String ssid = "Your SSID"; // 替换为要连接的 Wi-Fi 的 SSID
            String password = "Your Password"; // 替换为要连接的 Wi-Fi 的密码
            wifiUtils.connectToWifi(ssid, password);
            ToastUtil.showShort("正在连接到Wi-Fi");
        });

        binding.scanWifiBtn.setOnClickListener(v -> {
            wifiUtils.startScan();
        });

        binding.getWifiListBtn.setOnClickListener(v -> {
            List<ScanResult> wifiList = wifiUtils.getWifiList();
            // 处理 Wi-Fi 列表
            // 这里只是简单地将 Wi-Fi 列表输出到日志中
            for (ScanResult result : wifiList) {
                String wifiName = result.SSID;
                int signalLevel = result.level;
                Log.d("WifiList", "Wi-Fi Name: " + wifiName + ", Signal Level: " + signalLevel);
            }
        });

        binding.getCurrentWifiBtn.setOnClickListener(v -> {
            String currentWifiName = wifiUtils.getConnectedWifiName();
            if (currentWifiName != null) {
                ToastUtil.showShort("当前连接的Wi-Fi：" + currentWifiName);
            }
        });

        binding.getCurrentIpBtn.setOnClickListener(v -> {
            String currentIpAddress = wifiUtils.getIpAddress();
            if (currentIpAddress != null) {
                ToastUtil.showShort("当前IP地址：" + currentIpAddress);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wifiUtils.release();
        ToastUtil.cancelToast();
    }
}
