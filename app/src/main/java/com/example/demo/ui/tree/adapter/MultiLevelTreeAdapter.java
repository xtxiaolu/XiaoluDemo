package com.example.demo.ui.tree.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo.R;
import com.example.demo.model.MultiChildrenBean;
import com.example.library.tree.BaseTreeAdapter;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述:多级树 不固定层级 适配器
 */
public class MultiLevelTreeAdapter extends BaseTreeAdapter<MultiLevelTreeAdapter.VH, MultiChildrenBean> {
    private int selectPosition = -1;
    private final float fixLeft = 50;
    private Context mContext;
    private boolean isCollapsed = true;

    public MultiLevelTreeAdapter(Context context, List<MultiChildrenBean> list) {
        super(context, list);
        this.mContext = context;
    }

    public void setSelectPosition(int position){
        this.selectPosition=position;
        notifyDataSetChanged();
    }

    @Override
    public void onBindHolder(@androidx.annotation.NonNull VH holder, int position, MultiChildrenBean bean) {
        holder.tvName.setText(bean.getTypeName());
        if (bean.getChildren() != null && bean.getChildren().size() > 0){
            holder.ivNext.setVisibility(View.VISIBLE);
        }else {
            holder.ivNext.setVisibility(View.INVISIBLE);
        }
        if (bean.isOpen()){
            holder.ivNext.setRotation(90);
        }else {
            holder.ivNext.setRotation(0);
        }
        //fixLeft左边距
        int left = (int) ((bean.getLeave() + 1) * fixLeft);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.ivCheck.getLayoutParams();
        layoutParams.leftMargin = left;
        holder.ivCheck.setLayoutParams(layoutParams);
//        holder.ivCheck.setImageResource(bean.isCheck() ? R.drawable.road_checked : R.drawable.road_check);
        holder.ivCheck.setTag(position);
        holder.ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null){
                    onItemClickListener.onCheckClick(v,(int) v.getTag(),bean);
                }
            }
        });

        //为了让点击的item显示不同的颜色，必须在item点击后 重新 notifyDataSetChange
        if (position == selectPosition) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        } else {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white));
        }

        LinearLayout.LayoutParams itemLayoutParams = (LinearLayout.LayoutParams) holder.tvName.getLayoutParams();
        itemLayoutParams.leftMargin = left;
        holder.tvName.setLayoutParams(itemLayoutParams);
        holder.tvName.setTag(position);

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(v -> {
            if (onItemClickListener != null){
                selectPosition = position;
                onItemClickListener.onOpenChildClick(v,(int) v.getTag(),0,bean);
            }
        });
    }

    @androidx.annotation.NonNull
    @Override
    public VH onCreateViewHolder(@androidx.annotation.NonNull ViewGroup viewGroup, int i) {
        return new VH(getLayoutView(R.layout.item_multi_level_tree,viewGroup));
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onCheckClick(View v, int pos, MultiChildrenBean model);
        void onOpenChildClick(View v, int pos, int tag, MultiChildrenBean model);
    }
    public class VH extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView ivCheck;
        ImageView ivNext;
        public VH(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            ivCheck = itemView.findViewById(R.id.ivCheck);
            ivNext = itemView.findViewById(R.id.ivNext);
        }
    }
}

