package com.example.demo.ui.kotlin.adapter

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.example.demo.ui.kotlin.adapter.SelectableIAdapter.Companion.ITEM_TYPE_HEADER
import com.example.demo.ui.kotlin.adapter.SelectableIAdapter.Companion.ITEM_TYPE_ITEM

data class SelectableItem(
    val id: String,      // 项的唯一标识符
    val name: String,  // 项的名称
    var isSelected: Boolean, // 表示项是否被选中
    val isHeader: Boolean = false, // 是否是标题项，默认为普通项
    val headerText: String? = null // 标题项的文本，如果是标题项
) : MultiItemEntity {

    // 添加 hashCode 和 equals 方法，以便正确比较对象
    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        other as SelectableItem
        return id == other.id
    }

    override fun getItemType(): Int {
        return if (isHeader) {
            // 如果是标题项，返回标题项的类型值
            ITEM_TYPE_HEADER
        } else {
            // 否则返回普通项的类型值
            ITEM_TYPE_ITEM
        }
    }
}
