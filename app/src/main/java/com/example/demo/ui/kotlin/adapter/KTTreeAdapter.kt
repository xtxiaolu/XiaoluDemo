package com.example.demo.ui.kotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demo.R
import com.example.demo.databinding.ItemLevelTreeBinding
import com.example.demo.model.TreeItemBean
import com.example.demo.ui.tree.adapter.TreeAdapter

class KTTreeAdapter(private val mContext: Context, private val treeItemList: List<TreeItemBean>) :
    RecyclerView.Adapter<KTTreeAdapter.KTTreeViewHolder>() {

    private var selectPosition = RecyclerView.NO_POSITION
    private var onItemClickListener: OnItemClickListener? = null
//    private var itemClickListener: OnItemClickListener? = null
//    // 设置点击事件监听器的方法
//    fun setItemClickListener(listener: OnItemClickListener) {
//        this.itemClickListener = listener
//    }

    fun setSelectPosition(position: Int) {
        selectPosition = position
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KTTreeViewHolder {
        val binding = ItemLevelTreeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return KTTreeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: KTTreeViewHolder, position: Int) {
        val treeItem = treeItemList[position]
        holder.bind(treeItem)
    }

    override fun getItemCount(): Int {
        return treeItemList.size
    }

    inner class KTTreeViewHolder(val binding: ItemLevelTreeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(treeItem: TreeItemBean) {
            // 根据层级设置左边距
            val leftPadding = treeItem.level * 50
            binding.tvName.setPadding(leftPadding, 0, 0, 0)
            binding.tvName.text = treeItem.typeName

            // 根据展开状态设置箭头图标
            if (treeItem.children != null && treeItem.children.isNotEmpty()) {
                if (treeItem.isOpen) {
                    binding.ivNext.rotation = 90f
                } else {
                    binding.ivNext.rotation = 0f
                }
            } else {
                binding.ivNext.visibility = View.GONE
            }

            // 设置点击事件
            itemView.setOnClickListener {
                // 切换展开状态
                treeItem.isOpen = !treeItem.isOpen
                // 局部刷新，只刷新当前点击的位置和其子项的数据
                notifyItemChanged(bindingAdapterPosition)
                onItemClickListener?.onItemClick(itemView, bindingAdapterPosition, treeItem)
                setSelectPosition(bindingAdapterPosition)
            }

            // 如果没有数据子列表不显示
            binding.recyclerView.visibility = if (treeItem.isOpen) View.VISIBLE else View.GONE

            // 如果有子数据，设置适配器并显示箭头
            if (treeItem.children != null && treeItem.children.isNotEmpty()) {
                binding.ivNext.visibility = View.VISIBLE
                binding.recyclerView.visibility = if (treeItem.isOpen) View.VISIBLE else View.GONE
                // 计算子项的级别，加上适当的偏移量
                val childLevel = treeItem.level + 1
                treeItem.children.forEach { child ->
                    child.level = childLevel
                }
                binding.recyclerView.layoutManager = LinearLayoutManager(mContext)
                val childAdapter = TreeAdapter(
                    mContext,
                    treeItem.children
                )
                binding.recyclerView.adapter = childAdapter

                childAdapter.setOnItemClickListener { view, position, model ->
                    onItemClickListener?.onItemClick(view, position, model)
                }
            } else {    // 没有子数据，隐藏箭头
                binding.ivNext.visibility = View.GONE
                binding.recyclerView.visibility = View.GONE
            }

            // 根据是否带有右箭头来设置背景颜色
            itemView.setBackgroundColor(
                ContextCompat.getColor(
                    mContext,
                    if (binding.ivNext.visibility == View.VISIBLE
                        && bindingAdapterPosition == selectPosition
                    ) R.color.bg_default else R.color.white
                )
            )
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int, model: TreeItemBean)
    }
}