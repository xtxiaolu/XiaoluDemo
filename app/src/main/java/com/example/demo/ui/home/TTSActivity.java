package com.example.demo.ui.home;

import android.os.Bundle;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityTtsBinding;
import com.example.demo.model.UserBean;
import com.example.demo.util.TTSUtil;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/8/31
 * 描述: 用户检索列表
 */
public class TTSActivity extends BaseActivity {
    private ActivityTtsBinding binding;
    private TTSUtil ttsUtil;

    private List<UserBean> contactsList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTtsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String rqsSrc = "UserList.json";
        // 创建TTSUtil实例并传入Context和TTSListener
        ttsUtil = new TTSUtil(this, new TTSUtil.TTSListener() {
            @Override
            public void onInitSuccess() {
                // TTS引擎初始化成功
                // 这里可以进行语音合成操作
                String text = "初始化语音成功";
                ttsUtil.speak(text);
                Logger.e("TTS  TTSUtil  onInitSuccess 初始化语音成功");
            }

            @Override
            public void onInitFailure() {
                Logger.e("TTS  TTSUtil onInitFailure TTS引擎初始化失败");
            }

            @Override
            public void onSpeechStart() {
                Logger.e("TTS  TTSUtil onSpeechStart 语音合成开始");
            }

            @Override
            public void onSpeechDone() {
                Logger.e("TTS  TTSUtil onSpeechDone 语音合成完成");
            }

            @Override
            public void onSpeechError(String errorMessage) {
                Logger.e("TTS  TTSUtil onSpeechError 语音合成出错 " + errorMessage);
            }
        });

        binding.button.setOnClickListener(v -> {
            ttsUtil.speak(binding.textView.getText().toString().trim());
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ttsUtil.release();
    }
}