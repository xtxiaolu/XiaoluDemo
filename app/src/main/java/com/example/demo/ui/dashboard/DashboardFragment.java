package com.example.demo.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.demo.databinding.FragmentDashboardBinding;
import com.example.demo.ui.kotlin.KTreeActivity;
import com.example.demo.ui.tree.TreeActivity;

public class DashboardFragment extends Fragment {

    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initView();
        return root;
    }

    private void initView() {
        binding.button0.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), TreeActivity.class);
            startActivity(intent);
        });

        binding.button1.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), KTreeActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}