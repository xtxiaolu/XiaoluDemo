package com.example.demo.ui.tree;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityFixedLeveTreeBinding;
import com.example.demo.model.BaseBean;
import com.example.demo.model.ContactsBean;
import com.example.demo.ui.tree.adapter.FixedLeveTreeAdapter;
import com.example.demo.util.MyJson;
import com.example.library.utils.AssetsUtil;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述: 固定层级树
 * 使用三方控件 BaseRecyclerViewAdapterHelper 来完成
 */
public class FixedLeveTreeActivity extends BaseActivity {
    private ActivityFixedLeveTreeBinding binding;
    private List<ContactsBean.ChildrenBean> resultTreeData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFixedLeveTreeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String rqsJson = AssetsUtil.getAssetsJson(this,"FixedLeveTree.json");
        BaseBean dataList = MyJson.fromJson(rqsJson, BaseBean.class);
        List<ContactsBean.ChildrenBean> mChildren = MyJson.fromJson(MyJson.toJson(dataList.getData()),
                new TypeToken<List<ContactsBean.ChildrenBean>>() {}.getType());

        Logger.i("children数据: " + MyJson.toJson(mChildren));
        resultTreeData = refreshAdapter(mChildren);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        FixedLeveTreeAdapter treeAdapter = new FixedLeveTreeAdapter(this, resultTreeData);
        binding.recyclerView.setAdapter(treeAdapter);

        treeAdapter.setOnOneItemClickListener(new FixedLeveTreeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, ContactsBean.ChildrenBean tree) {
                Toast.makeText(FixedLeveTreeActivity.this,
                        "当前选择数据：" + tree.getDepartName(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private List<ContactsBean.ChildrenBean> refreshAdapter(List<ContactsBean.ChildrenBean> result) {
        return tree(result);
    }

    private List<ContactsBean.ChildrenBean> tree(List<ContactsBean.ChildrenBean> data) {
        List<ContactsBean.ChildrenBean> result = new ArrayList<>();

        for (ContactsBean.ChildrenBean child : data) {
            result.add(child);
            multilevelTree(result, child);
        }

        System.out.println("一共多少条数据: " + result.size());
        return result;
    }

    private void multilevelTree(List<ContactsBean.ChildrenBean> result, ContactsBean.ChildrenBean trees) {
        if (trees == null) {
            System.err.println("tree is null");
            return;
        }

        if (trees.getChildren() != null) {
            for (ContactsBean.ChildrenBean childTree : trees.getChildren()) {
                trees.addSubItem(childTree);
                multilevelTree(result, childTree);
            }
        }
    }
}