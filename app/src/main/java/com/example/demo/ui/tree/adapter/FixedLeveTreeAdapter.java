package com.example.demo.ui.tree.adapter;

import android.content.Context;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.demo.R;
import com.example.demo.model.ContactsBean;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/6/8
 * 描述:
 */
public class FixedLeveTreeAdapter extends BaseMultiItemQuickAdapter<ContactsBean.ChildrenBean, BaseViewHolder> {
    public static final int TYPE_LEVEL_0 = 1;
    public static final int TYPE_LEVEL_1 = 2;
    public static final int TYPE_LEVEL_2 = 3;
    private Context mContext;

    public FixedLeveTreeAdapter(Context context, List<ContactsBean.ChildrenBean> data) {
        super(data);
        this.mContext = context;
        addItemType(TYPE_LEVEL_0, R.layout.item_level_tree_1);
        addItemType(TYPE_LEVEL_1, R.layout.item_level_tree_2);
        addItemType(TYPE_LEVEL_2, R.layout.item_level_tree_3);
    }

    @Override
    protected void convert(BaseViewHolder holder, ContactsBean.ChildrenBean item) {
        switch (holder.getItemViewType()) {
            case TYPE_LEVEL_0:
                final ContactsBean.ChildrenBean lv0 = item;
                List<ContactsBean.ChildrenBean> Tree0 = lv0.getChildren();
                holder.setText(R.id.tv1, lv0.getDepartName());
                if (Tree0 == null){
                    holder.setVisible(R.id.ivArrow, false);
                    holder.setVisible(R.id.llDefect, false);
                    holder.setBackgroundRes(R.id.llDefect,R.color.white);
                } else {
                    holder.setVisible(R.id.ivArrow, true);
                    holder.setVisible(R.id.llDefect, true);
                    holder.setBackgroundRes(R.id.llDefect,R.color.bg_default);
                }

                if (lv0.isExpanded()) { // 开
                    holder.setBackgroundRes(R.id.ivArrow,R.drawable.arrow_b);
                } else { // 合
                    holder.setBackgroundRes(R.id.ivArrow,R.drawable.arrow_r);
                }

                int position = holder.getBindingAdapterPosition();
                holder.setOnClickListener(R.id.tv1, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOneItemClickListener.onItemClick(v,position,lv0);
                    }
                });

                holder.setOnClickListener(R.id.llDefect, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Tree0 == null){
                            onOneItemClickListener.onItemClick(v,position,lv0);
                        }
                        if (lv0.isExpanded()) { // 合
                            collapse(position);
                        } else { // 开
                            expand(position);
                        }
                    }
                });
                break;
            case TYPE_LEVEL_1:
                final ContactsBean.ChildrenBean lv1 = item;
                List<ContactsBean.ChildrenBean> Tree1 = lv1.getChildren();
                holder.setText(R.id.tv2, lv1.getDepartName());
                if (Tree1 == null){
                    holder.setVisible(R.id.ivArrow, false);
                    holder.setVisible(R.id.llDefect, false);
                    holder.setBackgroundRes(R.id.llDefect,R.color.white);
                } else {
                    holder.setVisible(R.id.ivArrow, true);
                    holder.setVisible(R.id.llDefect, true);
                    holder.setBackgroundRes(R.id.llDefect,R.color.bg_default);
                }

                if (lv1.isExpanded()) { // 开
                    holder.setBackgroundRes(R.id.ivArrow,R.drawable.arrow_b);
                } else { // 合
                    holder.setBackgroundRes(R.id.ivArrow,R.drawable.arrow_r);
                }

                holder.setOnClickListener(R.id.tv2, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOneItemClickListener.onItemClick(v,holder.getBindingAdapterPosition(),lv1);
                    }
                });

                holder.setOnClickListener(R.id.llDefect, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = holder.getBindingAdapterPosition();
                        if (Tree1 == null){
//                            ToastUtil.show("没有部门信息...");
                            onOneItemClickListener.onItemClick(v,holder.getBindingAdapterPosition(),lv1);
                        }
                        if (lv1.isExpanded()) { // 合
                            collapse(pos);
                        } else {  // 开
                            expand(pos);
                        }
                    }
                });
                break;
            case TYPE_LEVEL_2:
                final ContactsBean.ChildrenBean lv2 = item;
                holder.setText(R.id.tv3, lv2.getDepartName());

                holder.setOnClickListener(R.id.item, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOneItemClickListener.onItemClick(v,holder.getBindingAdapterPosition(),lv2);
                    }
                });
                break;
            default:
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, ContactsBean.ChildrenBean tree);
    }

    private OnItemClickListener onOneItemClickListener;

    public void setOnOneItemClickListener(OnItemClickListener onOneItemClickListener) {
        this.onOneItemClickListener = onOneItemClickListener;
    }
}
