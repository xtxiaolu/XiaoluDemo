package com.example.demo.ui.home;

import android.icu.text.SimpleDateFormat;
import android.os.Bundle;

import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityDateTimeBinding;
import com.example.demo.util.ToastUtil;
import com.example.library.utils.DateUtil;
import com.example.library.utils.TimeUtil;

import java.text.ParseException;
import java.util.Date;

/**
 * @author: xtxiaolu
 * @date: 2023/7/12
 * 描述: 时间格式与时间处理工具类测试
 */
public class DateTimeActivity extends BaseActivity {
    private ActivityDateTimeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDateTimeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.button1.setOnClickListener(view -> {
            ToastUtil.showShort("获取当前时间：" + DateUtil.getCurrentDate(DateUtil.FORMAT_YY_MM_DD));
        });

        binding.button2.setOnClickListener(view -> {
            ToastUtil.showShort("获取当前时间为本月的第几周：" + TimeUtil.getCurrentWeekOfMonth());
        });

        binding.button3.setOnClickListener(view -> {
            ToastUtil.showShort("获取当前时间为本周的第几天：" + TimeUtil.getCurrentDayOfWeek());
        });


        binding.button4.setOnClickListener(view -> {
            ToastUtil.showShort("返回当前日期是星期几：" + TimeUtil.getCurrentDayOfWeekText());
        });

        binding.button5.setOnClickListener(view -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startTime = null;
            Date endTime = null;
            Date currentTime = null;
            try {
                startTime = sdf.parse("2023-07-01 00:00:00");
                endTime = sdf.parse("2023-07-02 23:59:59");
                currentTime = sdf.parse("2023-07-02 12:00:00");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            boolean isInRange = TimeUtil.isInTimeRange(startTime, endTime, currentTime);
            ToastUtil.showShort("判断指定时间是否在时间区间内：" + isInRange);
        });


        binding.button6.setOnClickListener(view -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startTime = null;
            try {
                startTime = sdf.parse("2023-07-02 23:59:59");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            String s = TimeUtil.calculateTimeDifference(startTime);
            ToastUtil.showShort("返回友好时间跨度：" + s);
        });

        binding.button7.setOnClickListener(view -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startTime = null;
            Date endTime = null;
            try {
                startTime = sdf.parse("2023-07-01 00:00:00");
                endTime = sdf.parse("2023-07-14 23:59:59");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

            long daysDifference = TimeUtil.calculateDaysDifference(startTime, endTime);
            ToastUtil.showShort("日期相差天数：" + daysDifference);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.cancelToast();
    }
}
