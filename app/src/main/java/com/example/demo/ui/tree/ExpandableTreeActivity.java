package com.example.demo.ui.tree;

import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.example.demo.R;
import com.example.demo.bese.BaseActivity;
import com.example.demo.databinding.ActivityExpandableTreeBinding;
import com.example.demo.model.BaseBean;
import com.example.demo.model.DepartmentBean;
import com.example.demo.ui.tree.adapter.ExpandableAdapter;
import com.example.demo.util.MyJson;
import com.example.library.utils.AssetsUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述: 固定层级树
 * 使用 ExpandableListView 来完成
 */
public class ExpandableTreeActivity extends BaseActivity {
    private ActivityExpandableTreeBinding binding;
    private List<DepartmentBean> resultTreeData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExpandableTreeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String rqsJson = AssetsUtil.getAssetsJson(this,"FixedLeveTree.json");
        BaseBean dataList = MyJson.fromJson(rqsJson, BaseBean.class);
        List<DepartmentBean> mDepartment = MyJson.fromJson(MyJson.toJson(dataList.getData()),
                new TypeToken<List<DepartmentBean>>() {}.getType());

        ExpandableListView expandableListView = findViewById(R.id.expandableListView);
        ExpandableAdapter adapter = new ExpandableAdapter(mDepartment);
        expandableListView.setAdapter(adapter);


        // 设置父级点击事件
        expandableListView.setOnGroupClickListener((parent, v, groupPosition, id) -> {
//            Toast.makeText(this,"点击数据",Toast.LENGTH_LONG).show();
            return false; // 返回 true 表示已处理点击事件
        });

        // 设置子级点击事件
        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            Toast.makeText(this,"点击数据",Toast.LENGTH_LONG).show();
            return true; // 返回 true 表示已处理点击事件
        });

        adapter.setOnItemClickListener(new ExpandableAdapter.OnItemClickListener() {
            @Override
            public void onGroupItemClick(int groupPosition, Object o) {
                DepartmentBean depart = (DepartmentBean) o;
                Toast.makeText(ExpandableTreeActivity.this,
                        depart.getDepartName(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildItemClick(int groupPosition, int childPosition, Object o) {
                DepartmentBean depart = (DepartmentBean) o;
                Toast.makeText(ExpandableTreeActivity.this,
                        depart.getDepartName(),Toast.LENGTH_LONG).show();
            }
        });
    }
}