package com.example.demo.ui.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.chad.library.adapter.base.BaseViewHolder;
import com.example.demo.R;
import com.example.demo.databinding.FragmentHomeBinding;
import com.example.demo.model.BaseBean;
import com.example.demo.model.SpinnerBean;
import com.example.demo.ui.kotlin.SelectableActivity;
import com.example.demo.ui.photo.PhotoSelectorActivity;
import com.example.demo.ui.tree.ExpandableTreeActivity;
import com.example.demo.ui.tree.FixedLeveTreeActivity;
import com.example.demo.ui.tree.MultiLevelTreeActivity;
import com.example.demo.util.MyJson;
import com.example.demo.view.CustomSpinner;
import com.example.library.utils.AssetsUtil;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private List<SpinnerBean> spinnerData = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initView();
        return root;
    }

    private void initView() {
        String rqsJson = AssetsUtil.getAssetsJson(getActivity(),"spdemo.json");
        BaseBean data = MyJson.fromJson(rqsJson, BaseBean.class);
        spinnerData = MyJson.fromJson(MyJson.toJson(data.getData()), new TypeToken<List<SpinnerBean>>() {
        }.getType());


        binding.customSpinner.setData(new ArrayList<>());
        binding.customSpinner.setOnItemSelectedListener(new CustomSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position, Object item) {
                SpinnerBean subjects = (SpinnerBean) item;
                binding.customSpinner.setTextViewValue(subjects.getName());
                // 选择后的示例，剩下需要自己扩展
            }

            @Override
            public void onItemCallBackData(BaseViewHolder holder, Object item) {
                SpinnerBean subjects = (SpinnerBean) item;
                TextView popwindTv = holder.getView(R.id.listview_popwind_tv);
                popwindTv.setText(subjects.getName());
                TextView textView = binding.customSpinner.getTextViewValue();
                if (textView.getText().toString().trim().equals(subjects.getName())) {
                    popwindTv.setTextColor(Color.parseColor("#00d8a0"));
                    holder.getView(R.id.iv_select_icon).setVisibility(View.VISIBLE);
                } else {
                    popwindTv.setTextColor(Color.parseColor("#333333"));
                    holder.getView(R.id.iv_select_icon).setVisibility(View.GONE);
                }
            }
        });
        binding.customSpinner.replaceData(spinnerData);

        binding.button1.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), MultiLevelTreeActivity.class);
            startActivity(intent);
        });

        binding.button2.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), FixedLeveTreeActivity.class);
            startActivity(intent);
        });

        binding.button3.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), ExpandableTreeActivity.class);
            startActivity(intent);
        });

        binding.button4.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), WifiDemoActivity.class);
            startActivity(intent);
        });

        binding.button5.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), DateTimeActivity.class);
            startActivity(intent);
        });

        binding.button6.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), HandlerTextActivity.class);
            startActivity(intent);
        });

        binding.button7.setOnClickListener(view -> {

        });

        binding.button8.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), CounterViewActivity.class);
            startActivity(intent);
        });

        binding.button9.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), SelectableActivity.class);
            startActivity(intent);
        });

        binding.button10.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), PhotoSelectorActivity.class);
            startActivity(intent);
        });

        binding.button11.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), UserRetrievalActivity.class);
            startActivity(intent);
        });

        binding.button12.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), TTSActivity.class);
            startActivity(intent);
        });

        binding.button13.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getActivity(), VideoActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}