package com.example.demo.ui.kotlin.adapter

import android.util.SparseBooleanArray
import android.widget.CheckBox
import android.widget.LinearLayout
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.example.demo.R

class SelectableIAdapter(data: List<MultiItemEntity>) : BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(data) {
    private var itemClickListener: OnItemsClickListener? = null
    // 为解决滑动时数据错乱问题将数据添加到集合中
    private val selectedStatus = SparseBooleanArray()

    // 设置点击事件监听器的方法
    fun setItemClickListener(listener: OnItemsClickListener) {
        this.itemClickListener = listener
    }

    companion object {
        const val ITEM_TYPE_HEADER = 0
        const val ITEM_TYPE_ITEM = 1
    }

    private var isAllSelected = false

    init {
        addItemType(ITEM_TYPE_HEADER, R.layout.item_header) // 添加头部视图
        addItemType(ITEM_TYPE_ITEM, R.layout.item_selectable_item) // 添加底部视图
    }

    override fun convert(holder: BaseViewHolder, item: MultiItemEntity) {
        val position = holder.absoluteAdapterPosition
        when (holder.itemViewType) {
            ITEM_TYPE_HEADER -> { // 处理头部数据
                // 处理头部数据
                holder.getView<LinearLayout>(R.id.header_layout).setOnClickListener { pos ->
                    // 点击事件
                    itemClickListener?.onHeaderClick(position)
                }
            }
            ITEM_TYPE_ITEM -> {
                val selectableItem = item as SelectableItem
                holder.setText(R.id.tvHeader, selectableItem.name)
                val checkBox = holder.getView<CheckBox>(R.id.checkBox)

                // 设置CheckBox的状态
                checkBox.isChecked = selectableItem.isSelected

                holder.itemView.setOnClickListener {
                    // 单击时切换选择状态
                    selectableItem.isSelected = !selectableItem.isSelected
                    checkBox.isChecked = selectableItem.isSelected

                    // 更新selectedStatus中的选择状态
                    selectedStatus.put(position, selectableItem.isSelected)
                    itemClickListener?.onItemButtonClick(position)
                }
            }
        }
    }

    // 全选
    fun selectAll() {
        isAllSelected = true
        selectedStatus.clear()
        for (i in 0 until data.size) {
            val item = data[i] as SelectableItem
            if (item != null && !item.isHeader) { // 仅添加非头部项
                item.isSelected = true
                selectedStatus.put(i, true)
            }
        }
        notifyDataSetChanged()
    }

    // 取消全选/已选择
    fun clearSelection() {
        isAllSelected = false
        selectedStatus.clear()
        for (i in 0 until data.size) {
            val item = data[i] as SelectableItem
            item.isSelected = false
        }
        notifyDataSetChanged()
    }

    // 获取已选择了多少条数据
    fun getSelectedItemCount(): Int {
        return data.count { it is SelectableItem && it.isSelected }
    }

    // 获取选择后的数据集合
    fun getSelectedList(): List<SelectableItem> {
        val selectedItems = mutableListOf<SelectableItem>()
        for (i in 0 until selectedStatus.size()) {
            val position = selectedStatus.keyAt(i)
            val item = data[position] as? SelectableItem
            if (item != null && selectedStatus.get(position)) {
                selectedItems.add(item)
            }
        }
        return selectedItems
    }
}