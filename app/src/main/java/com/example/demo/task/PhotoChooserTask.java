package com.example.demo.task;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import com.example.demo.R;
import com.example.demo.bese.BaseActivity;
import com.example.library.base.MBaseActivity;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.util.HashMap;
import java.util.List;

public class PhotoChooserTask extends ChooserBase<IntentResult> {

    public PhotoChooserTask(BaseActivity activity) {
        super(activity);
    }

    @Override
    public void show() {
        HashMap<String, Boolean> map = new HashMap<>();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
//            map.put(Manifest.permission.READ_MEDIA_IMAGES, true);
//            map.put(Manifest.permission.READ_MEDIA_VIDEO, true);
//            map.put(Manifest.permission.READ_MEDIA_AUDIO, true);
//        } else {
//            map.put(Manifest.permission.READ_EXTERNAL_STORAGE, true);
//            map.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
//        }
        map.put(Manifest.permission.READ_MEDIA_IMAGES, true);
        map.put(Manifest.permission.READ_MEDIA_VIDEO, true);
        map.put(Manifest.permission.READ_MEDIA_AUDIO, true);
        map.put(Manifest.permission.CAMERA, true);
        BaseActivity baseActivity = getActivity();
        baseActivity.checkAndRequestPermissions(map, new MBaseActivity.PermissionCallback() {
            @Override
            public void onGranted(int type, List<String> permissionsGranted) {
                Matisse.from(getActivity())
                        .choose(MimeType.of(MimeType.PNG, MimeType.JPEG))
                        .theme(R.style.My_Matisse_Zhihu)
                        .capture(true)
                        .maxSelectable(9)
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        .thumbnailScale(0.85f)
                        .captureStrategy(new CaptureStrategy(true, getActivity().getPackageName() + ".fileprovider"))
                        .imageEngine(new PicassoEngine())
                        .forResult(ChooserConstant.REQUEST_CODE_PHOTO_TASK);
            }

            @Override
            public boolean onReject() {
                OnCompletionListener<IntentResult> listener = getOnCompletionListener();
                if (listener != null) {
                    listener.onCompletion(new IntentResult(Activity.RESULT_CANCELED, new Intent()));
                }
                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ChooserConstant.REQUEST_CODE_PHOTO_TASK) {
            OnCompletionListener<IntentResult> listener = getOnCompletionListener();
            if (listener != null) {
                listener.onCompletion(new IntentResult(resultCode, data));
            }
        }
    }
}
