package com.example.demo.task;

import android.content.Intent;

/**
 * ActivityResult 结果包装
 * Created by xtxiaolu on 2023/8/30.
 */
public class IntentResult {

    public final int resultCode;
    public final Intent data;

    IntentResult(int resultCode, Intent data) {
        this.resultCode = resultCode;
        this.data = data;
    }
}
