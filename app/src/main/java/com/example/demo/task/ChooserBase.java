package com.example.demo.task;

import android.content.Intent;

import com.example.demo.bese.BaseActivity;

/**
 * 任务选择基类
 *
 * @param <T>
 */
public abstract class ChooserBase<T> {

    public interface OnCompletionListener <T> {
        void onCompletion(T result);
    }

    private OnCompletionListener<T> mOnCompletionListener;

    private BaseActivity activity;

    public ChooserBase(BaseActivity activity) {
        this.activity = activity;
    }

    public BaseActivity getActivity() {
        return activity;
    }

    public abstract void show();

    public abstract void onActivityResult(int requestCode, int resultCode, Intent data);

    public ChooserBase onCompleted(OnCompletionListener<T> listener) {
        mOnCompletionListener = listener;
        return this;
    }

    public OnCompletionListener<T> getOnCompletionListener() {
        return mOnCompletionListener;
    }
}
