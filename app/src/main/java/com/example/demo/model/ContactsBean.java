package com.example.demo.model;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: xiaolu
 * @date: 2021/8/3
 * 部门 信息
 */
public class ContactsBean implements Serializable {


    /**
     * dataList : [{"open":true,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"583bf000b9fa11eb876a44a84233a5df","departName":"厂领导","pId":"D1","departType":null,"treeId":"/D1/583bf000b9fa11eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"test","createTime":"2021-05-21 01:04","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"洗煤厂管理员","modifyTime":"2021-06-11 01:27"}],"departId":"D1","departName":"王家塔选煤厂","pId":"0","departType":null,"treeId":"/D1/","locking":1,"treeLevel":1,"childnum":null,"validity":1,"companyId":1,"createUserId":null,"createUserName":null,"createTime":null,"modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"洗煤厂管理员","modifyTime":"2021-06-11 01:25"},{"open":true,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"48f56bc9cd7a11eb876a44a84233a5df","departName":"厂领导01","pId":"9e1a93cebec011eb876a44a84233a5df","departType":null,"treeId":"/9e1a93cebec011eb876a44a84233a5df/48f56bc9cd7a11eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-06-14 20:37","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-10 18:13"}],"departId":"9e1a93cebec011eb876a44a84233a5df","departName":"王家塔选煤厂","pId":"0","departType":null,"treeId":"/9e1a93cebec011eb876a44a84233a5df/","locking":0,"treeLevel":1,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-05-27 02:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-14 09:41"},{"open":true,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"3d6ac131e51a11eb876a44a84233a5df","departName":"test","pId":"c5380538e51011eb876a44a84233a5df","departType":null,"treeId":"/c5380538e51011eb876a44a84233a5df/3d6ac131e51a11eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 11:10","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 11:10"}],"departId":"c5380538e51011eb876a44a84233a5df","departName":"yyztest","pId":"0","departType":null,"treeId":"/c5380538e51011eb876a44a84233a5df/","locking":0,"treeLevel":1,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:02","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:02"},{"open":true,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"0bd8810de51911eb876a44a84233a5df","departName":"嗷 嗷","pId":"d0cfb15de51011eb876a44a84233a5df","departType":null,"treeId":"/d0cfb15de51011eb876a44a84233a5df/0bd8810de51911eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 11:02","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 11:07"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"14817a9be51911eb876a44a84233a5df","departName":"2223","pId":"d0cfb15de51011eb876a44a84233a5df","departType":null,"treeId":"/d0cfb15de51011eb876a44a84233a5df/14817a9be51911eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 11:02","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-27 10:02"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"c2e5fe4ee51911eb876a44a84233a5df","departName":"嗷                           嗷","pId":"d0cfb15de51011eb876a44a84233a5df","departType":null,"treeId":"/d0cfb15de51011eb876a44a84233a5df/c2e5fe4ee51911eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 11:07","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 11:07"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"36aba286e53511eb876a44a84233a5df","departName":"test1","pId":"d0cfb15de51011eb876a44a84233a5df","departType":null,"treeId":"/d0cfb15de51011eb876a44a84233a5df/36aba286e53511eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 14:23","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 14:23"}],"departId":"d0cfb15de51011eb876a44a84233a5df","departName":"yyztest","pId":"0","departType":null,"treeId":"/d0cfb15de51011eb876a44a84233a5df/","locking":0,"treeLevel":1,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:03","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:03"},{"open":true,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"f10746c1e51711eb876a44a84233a5df","departName":"123456","pId":"ecff3627e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/db0db4c0e51711eb876a44a84233a5df/df9da08fe51711eb876a44a84233a5df/e3632437e51711eb876a44a84233a5df/ecff3627e51711eb876a44a84233a5df/f10746c1e51711eb876a44a84233a5df/","locking":0,"treeLevel":13,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:54","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:54"}],"departId":"ecff3627e51711eb876a44a84233a5df","departName":"123456","pId":"e3632437e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/db0db4c0e51711eb876a44a84233a5df/df9da08fe51711eb876a44a84233a5df/e3632437e51711eb876a44a84233a5df/ecff3627e51711eb876a44a84233a5df/","locking":0,"treeLevel":12,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:54","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:54"}],"departId":"e3632437e51711eb876a44a84233a5df","departName":"123456","pId":"df9da08fe51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/db0db4c0e51711eb876a44a84233a5df/df9da08fe51711eb876a44a84233a5df/e3632437e51711eb876a44a84233a5df/","locking":0,"treeLevel":11,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"df9da08fe51711eb876a44a84233a5df","departName":"123456","pId":"db0db4c0e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/db0db4c0e51711eb876a44a84233a5df/df9da08fe51711eb876a44a84233a5df/","locking":0,"treeLevel":10,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"db0db4c0e51711eb876a44a84233a5df","departName":"123456","pId":"d28fc7c4e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/db0db4c0e51711eb876a44a84233a5df/","locking":0,"treeLevel":9,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"d28fc7c4e51711eb876a44a84233a5df","departName":"123456","pId":"d04ad32ae51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/d28fc7c4e51711eb876a44a84233a5df/","locking":0,"treeLevel":8,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"d04ad32ae51711eb876a44a84233a5df","departName":"123456","pId":"cda42e99e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/d04ad32ae51711eb876a44a84233a5df/","locking":0,"treeLevel":7,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"cda42e99e51711eb876a44a84233a5df","departName":"123456","pId":"ca914e40e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/cda42e99e51711eb876a44a84233a5df/","locking":0,"treeLevel":6,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"ca914e40e51711eb876a44a84233a5df","departName":"123456","pId":"c63f7530e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/ca914e40e51711eb876a44a84233a5df/","locking":0,"treeLevel":5,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"c63f7530e51711eb876a44a84233a5df","departName":"123456","pId":"c3164947e51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/c63f7530e51711eb876a44a84233a5df/","locking":0,"treeLevel":4,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:53","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:53"}],"departId":"c3164947e51711eb876a44a84233a5df","departName":"123456","pId":"7103f52be51711eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/c3164947e51711eb876a44a84233a5df/","locking":0,"treeLevel":3,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:52","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:52"}],"departId":"7103f52be51711eb876a44a84233a5df","departName":"123456","pId":"1af0c7f4e51111eb876a44a84233a5df","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/7103f52be51711eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:50","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:50"}],"departId":"1af0c7f4e51111eb876a44a84233a5df","departName":"123456","pId":"0","departType":null,"treeId":"/1af0c7f4e51111eb876a44a84233a5df/","locking":0,"treeLevel":1,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"选煤厂管理员","createTime":"2021-07-15 10:05","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"选煤厂管理员","modifyTime":"2021-07-15 10:29"}]
     * msg :
     * status : 200
     */

    private String msg;
    private Integer status;
    private List<ChildrenBean> dataList = new ArrayList<>();

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ChildrenBean> getDataList() {
        return dataList;
    }

    public void setDataList(List<ChildrenBean> dataList) {
        this.dataList = dataList;
    }

    public class ChildrenBean extends AbstractExpandableItem<ChildrenBean> implements MultiItemEntity {
        /**
         * open : true
         * pName : null
         * oldName : null
         * children : [{"open":false,"pName":null,"oldName":null,"children":null,"departId":"583bf000b9fa11eb876a44a84233a5df","departName":"厂领导","pId":"D1","departType":null,"treeId":"/D1/583bf000b9fa11eb876a44a84233a5df/","locking":0,"treeLevel":2,"childnum":0,"validity":1,"companyId":1,"createUserId":"8ab7481950b314b20150b32cc9c9089f","createUserName":"test","createTime":"2021-05-21 01:04","modifyUserId":"8ab7481950b314b20150b32cc9c9089f","modifyUserName":"洗煤厂管理员","modifyTime":"2021-06-11 01:27"}]
         * departId : D1
         * departName : 王家塔选煤厂
         * pId : 0
         * departType : null
         * treeId : /D1/
         * locking : 1
         * treeLevel : 1
         * childnum : null
         * validity : 1
         * companyId : 1
         * createUserId : null
         * createUserName : null
         * createTime : null
         * modifyUserId : 8ab7481950b314b20150b32cc9c9089f
         * modifyUserName : 洗煤厂管理员
         * modifyTime : 2021-06-11 01:25
         */

        private Boolean open;
        private Object pName;
        private Object oldName;
        private String departId;
        private String departName;
        private String pId;
        private Object departType;
        private String treeId;
        private Integer locking;
        private Integer treeLevel;
        private Object childnum;
        private Integer validity;
        private Integer companyId;
        private Object createUserId;
        private String createUserName;
        private Object createTime;
        private String modifyUserId;
        private String modifyUserName;
        private String modifyTime;
        private String principalIds;
        private String principalNames;
        private List<ChildrenBean> children;

        private int markPosition = -1;
        private boolean isChecked = false; // 是否选择  用于选择页面数据
        private boolean isSelect = false; // 是否勾选  用于 操作dialog页面数据  本地数据操作字段
        private boolean isAllSelect = false; // 确认选择  用于 操作别的页面数据  本地数据操作字段
        private boolean isMoreSelect = false; // 是否是多选  true:多选  false:单选

        public int getMarkPosition() {
            return markPosition;
        }

        public void setMarkPosition(int markPosition) {
            this.markPosition = markPosition;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        public boolean isAllSelect() {
            return isAllSelect;
        }

        public void setAllSelect(boolean allSelect) {
            isAllSelect = allSelect;
        }

        public Boolean isOpen() {
            return open;
        }

        public void setOpen(Boolean open) {
            this.open = open;
        }

        public Object getPName() {
            return pName;
        }

        public void setPName(Object pName) {
            this.pName = pName;
        }

        public Object getOldName() {
            return oldName;
        }

        public void setOldName(Object oldName) {
            this.oldName = oldName;
        }

        public String getDepartId() {
            return departId;
        }

        public void setDepartId(String departId) {
            this.departId = departId;
        }

        public String getDepartName() {
            return departName;
        }

        public void setDepartName(String departName) {
            this.departName = departName;
        }

        public String getPId() {
            return pId;
        }

        public void setPId(String pId) {
            this.pId = pId;
        }

        public Object getDepartType() {
            return departType;
        }

        public void setDepartType(Object departType) {
            this.departType = departType;
        }

        public String getTreeId() {
            return treeId;
        }

        public void setTreeId(String treeId) {
            this.treeId = treeId;
        }

        public Integer getLocking() {
            return locking;
        }

        public void setLocking(Integer locking) {
            this.locking = locking;
        }

        public Integer getTreeLevel() {
            return treeLevel;
        }

        public void setTreeLevel(Integer treeLevel) {
            this.treeLevel = treeLevel;
        }

        public Object getChildnum() {
            return childnum;
        }

        public void setChildnum(Object childnum) {
            this.childnum = childnum;
        }

        public Integer getValidity() {
            return validity;
        }

        public void setValidity(Integer validity) {
            this.validity = validity;
        }

        public Integer getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        public Object getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(Object createUserId) {
            this.createUserId = createUserId;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public String getModifyUserId() {
            return modifyUserId;
        }

        public void setModifyUserId(String modifyUserId) {
            this.modifyUserId = modifyUserId;
        }

        public String getModifyUserName() {
            return modifyUserName;
        }

        public void setModifyUserName(String modifyUserName) {
            this.modifyUserName = modifyUserName;
        }

        public String getModifyTime() {
            return modifyTime;
        }

        public void setModifyTime(String modifyTime) {
            this.modifyTime = modifyTime;
        }

        public List<ChildrenBean> getChildren() {
            return children;
        }

        public void setChildren(List<ChildrenBean> children) {
            this.children = children;
        }
        public String getPrincipalIds() {
            return principalIds;
        }

        public void setPrincipalIds(String principalIds) {
            this.principalIds = principalIds;
        }

        public String getPrincipalNames() {
            return principalNames;
        }

        public void setPrincipalNames(String principalNames) {
            this.principalNames = principalNames;
        }

        @Override
        public String toString() {
            return "DataListBean{" +
                    "open=" + open +
                    ", pName=" + pName +
                    ", oldName=" + oldName +
                    ", departId='" + departId + '\'' +
                    ", departName='" + departName + '\'' +
                    ", pId='" + pId + '\'' +
                    ", departType=" + departType +
                    ", treeId='" + treeId + '\'' +
                    ", locking=" + locking +
                    ", treeLevel=" + treeLevel +
                    ", childnum=" + childnum +
                    ", validity=" + validity +
                    ", companyId=" + companyId +
                    ", createUserId=" + createUserId +
                    ", createUserName=" + createUserName +
                    ", createTime=" + createTime +
                    ", modifyUserId='" + modifyUserId + '\'' +
                    ", modifyUserName='" + modifyUserName + '\'' +
                    ", modifyTime='" + modifyTime + '\'' +
                    ", children=" + children +
                    '}'+ '\n';
        }

        @Override
        public int getLevel() {
            return treeLevel;
        }

        @Override
        public int getItemType() {
            return treeLevel;
        }
    }

    private ChildrenBean tree;

    public ChildrenBean getLevel() {
        return tree;
    }

    public ChildrenBean getItemType() {
        return tree = tree;
    }

    public ChildrenBean getTree() {
        return tree;
    }

    public void setTree(ChildrenBean tree) {
        this.tree = tree;
    }


    @Override
    public String toString() {
        return "TttBean{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", dataList=" + dataList +
                '}';
    }
}