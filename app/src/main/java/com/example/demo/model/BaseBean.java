package com.example.demo.model;

import java.io.Serializable;

public class BaseBean<T> implements Serializable {
    private boolean success;
    private boolean rel;
    public int status;

    public T data;
    public T dataList;

    public int getCode() {
        return status;
    }

    public void setCode(int code) {
        this.status = code;
    }

    public boolean isSuccess() {
        return status == 200;
    }

    public T getData() {
        if (data != null) {
            return data;
        }
        return dataList;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setDataList(T data) {
        this.dataList = data;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isRel() {
        return rel;
    }

    public void setRel(boolean rel) {
        this.rel = rel;
    }

    @Override
    public String toString() {
        return "BaseBean{" +
                "success=" + success +
                ", rel=" + rel +
                ", status=" + status +
                ", data=" + data +
                '}';
    }
}
