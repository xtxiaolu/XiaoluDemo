package com.example.demo.model

data class VideoBean(
    val id: Int,
    val name: String,
    val introduce: String,
    val channels: List<ChannelBean>
)
