package com.example.demo.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/10
 * 描述:
 */
public class DepartmentBean implements Serializable {

    /**
     * open : true
     * pName : null
     * oldName : null
     * children : [{"open":false,"pName":null,"oldName":null,"children":null,"departId":"9exz89mes0nz7opxwyysnn13vdhna6a8","departName":"厂部测试","fullName":"总部门/厂部测试","pId":"D1","departType":null,"treeId":"/D1/9exz89mes0nz7opxwyysnn13vdhna6a8/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:28"},{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"4s6aku2bripr0ficmnj7xams8tfs1ovg","departName":"测试一班","fullName":"总部门/测试生产/测试一班","pId":"rc55qit3sooerxg94fj5rvysd4c3smf1","departType":null,"treeId":"/D1/rc55qit3sooerxg94fj5rvysd4c3smf1/4s6aku2bripr0ficmnj7xams8tfs1ovg/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2022-11-02 17:01"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"9atb2z6t7lmekur28cp2pwikglv8qfs1","departName":"测试二班","fullName":"总部门/测试生产/测试二班","pId":"rc55qit3sooerxg94fj5rvysd4c3smf1","departType":null,"treeId":"/D1/rc55qit3sooerxg94fj5rvysd4c3smf1/9atb2z6t7lmekur28cp2pwikglv8qfs1/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2022-11-02 17:01"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"cag9ar0dsrtvazvo3zpttiqbdd5u565q","departName":"测试三班","fullName":"总部门/测试生产/测试三班","pId":"rc55qit3sooerxg94fj5rvysd4c3smf1","departType":null,"treeId":"/D1/rc55qit3sooerxg94fj5rvysd4c3smf1/cag9ar0dsrtvazvo3zpttiqbdd5u565q/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2022-11-02 17:01"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"nfd2518mlccwo7lhh6i7r9esliadugsm","departName":"测试四班","fullName":"总部门/测试生产/测试四班","pId":"rc55qit3sooerxg94fj5rvysd4c3smf1","departType":null,"treeId":"/D1/rc55qit3sooerxg94fj5rvysd4c3smf1/nfd2518mlccwo7lhh6i7r9esliadugsm/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-11-08 10:13"}],"departId":"rc55qit3sooerxg94fj5rvysd4c3smf1","departName":"测试生产","fullName":"总部门/测试生产","pId":"D1","departType":null,"treeId":"/D1/rc55qit3sooerxg94fj5rvysd4c3smf1/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:29"},{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"0f26o92l1a7c40cjaqkwounkool3f4s4","departName":"机电一部","fullName":"总部门/机电部/机电一部","pId":"zfeadh7ciw874okt4s6d9cker7vjhuun","departType":null,"treeId":"/D1/zfeadh7ciw874okt4s6d9cker7vjhuun/0f26o92l1a7c40cjaqkwounkool3f4s4/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2021-10-10 14:17"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"pzsla2p9p4rx2t1sd0dhqymphhwghvpd","departName":"机电二部","fullName":"总部门/机电部/机电二部","pId":"zfeadh7ciw874okt4s6d9cker7vjhuun","departType":null,"treeId":"/D1/zfeadh7ciw874okt4s6d9cker7vjhuun/pzsla2p9p4rx2t1sd0dhqymphhwghvpd/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:30"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"7rpl7yjf263na01ef3ukjjnvm30x79xh","departName":"机电三部","fullName":"总部门/机电部/机电三部","pId":"zfeadh7ciw874okt4s6d9cker7vjhuun","departType":null,"treeId":"/D1/zfeadh7ciw874okt4s6d9cker7vjhuun/7rpl7yjf263na01ef3ukjjnvm30x79xh/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:30"}],"departId":"zfeadh7ciw874okt4s6d9cker7vjhuun","departName":"机电部","fullName":"总部门/机电部","pId":"D1","departType":null,"treeId":"/D1/zfeadh7ciw874okt4s6d9cker7vjhuun/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:30"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"90bkfdm1xibqu40f5ndsh7lk5poirbb9","departName":"安全部","fullName":"总部门/安全部","pId":"D1","departType":null,"treeId":"/D1/90bkfdm1xibqu40f5ndsh7lk5poirbb9/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:31"},{"open":false,"pName":null,"oldName":null,"children":[{"open":false,"pName":null,"oldName":null,"children":null,"departId":"7d0eay4qsz1ws8lmai3j6da8ih7rzp27","departName":"总测试","fullName":"总部门/综合测试/总测试","pId":"3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj","departType":null,"treeId":"/D1/3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj/7d0eay4qsz1ws8lmai3j6da8ih7rzp27/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2021-10-10 14:21"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"k4d1jqguqbr4l2vcuo2j2ddlk2esvhfl","departName":"分部测试","fullName":"总部门/综合测试/分部测试","pId":"3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj","departType":null,"treeId":"/D1/3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj/k4d1jqguqbr4l2vcuo2j2ddlk2esvhfl/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2021-10-10 14:53"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"1g1tsbn30a4wlo001opmgegc55t5qsr2","departName":"综合办","fullName":"总部门/综合测试/综合办","pId":"3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj","departType":null,"treeId":"/D1/3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj/1g1tsbn30a4wlo001opmgegc55t5qsr2/","locking":0,"treeLevel":3,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-11-08 10:22"}],"departId":"3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj","departName":"综合测试","fullName":"总部门/综合测试","pId":"D1","departType":null,"treeId":"/D1/3nuuj2osc0uvfq8qfz2zg2qbsi3tcnwj/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"blq44rn8zq9ryajuazer5m3bhweei53q","modifyUserName":"系统管理员","modifyTime":"2022-10-01 14:31"},{"open":false,"pName":null,"oldName":null,"children":null,"departId":"rg3q3dirh4bf1zm1h1n7ev8oep4434g2","departName":"测试班组","fullName":"总部门/测试班组","pId":"D1","departType":null,"treeId":"/D1/rg3q3dirh4bf1zm1h1n7ev8oep4434g2/","locking":0,"treeLevel":2,"childnum":0,"modifyUserId":"ktypjzlclvv1mpucbm64lmxgwjeom8pe","modifyUserName":"系统管理员","modifyTime":"2022-11-02 17:03"}]
     * departId : D1
     * departName : 总部门
     * fullName : 总部门
     * pId : 0
     * departType : null
     * treeId : /D1/
     * locking : 1
     * treeLevel : 1
     * childnum : 0
     * modifyUserId : blq44rn8zq9ryajuazer5m3bhweei53q
     * modifyUserName : 系统管理员
     * modifyTime : 2022-10-31 10:29
     */

    private boolean open;
    private Object pName;
    private Object oldName;
    private String departId;
    private String departName;
    private String fullName;
    private String pId;
    private Object departType;
    private String treeId;
    private int locking;
    private int treeLevel;
    private int childnum;
    private String modifyUserId;
    private String modifyUserName;
    private String modifyTime;
    private List<DepartmentBean> children;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Object getPName() {
        return pName;
    }

    public void setPName(Object pName) {
        this.pName = pName;
    }

    public Object getOldName() {
        return oldName;
    }

    public void setOldName(Object oldName) {
        this.oldName = oldName;
    }

    public String getDepartId() {
        return departId;
    }

    public void setDepartId(String departId) {
        this.departId = departId;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPId() {
        return pId;
    }

    public void setPId(String pId) {
        this.pId = pId;
    }

    public Object getDepartType() {
        return departType;
    }

    public void setDepartType(Object departType) {
        this.departType = departType;
    }

    public String getTreeId() {
        return treeId;
    }

    public void setTreeId(String treeId) {
        this.treeId = treeId;
    }

    public int getLocking() {
        return locking;
    }

    public void setLocking(int locking) {
        this.locking = locking;
    }

    public int getTreeLevel() {
        return treeLevel;
    }

    public void setTreeLevel(int treeLevel) {
        this.treeLevel = treeLevel;
    }

    public int getChildnum() {
        return childnum;
    }

    public void setChildnum(int childnum) {
        this.childnum = childnum;
    }

    public String getModifyUserId() {
        return modifyUserId;
    }

    public void setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public List<DepartmentBean> getChildren() {
        return children;
    }

    public void setChildren(List<DepartmentBean> children) {
        this.children = children;
    }
}
