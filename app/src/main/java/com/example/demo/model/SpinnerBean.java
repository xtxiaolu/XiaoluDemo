package com.example.demo.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/10
 * 描述:
 */
public class SpinnerBean implements Serializable {

    /**
     * id : 1
     * name : 测试1
     * subjectName : 测试2
     */

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
