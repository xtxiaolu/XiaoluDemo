package com.example.demo.model

data class ChannelBean(
    val id: Int,
    val address: String,
    val name: String,
    val introduce: String,
    val model: Any
)
