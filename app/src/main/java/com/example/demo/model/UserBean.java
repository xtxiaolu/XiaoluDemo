package com.example.demo.model;

import java.io.Serializable;

/**
 * @author: xiaolu
 * @date: 2021/8/3
 * 部门 信息
 */
public class UserBean implements Serializable {

    private String id;
    private String realName;
    private String userPost;
    private Object userIcon;
    private String organizationId;
    private String userPhone;
    private String userWorkStatus;
    private String organizationName;
    private String letters;//显示拼音的首字母

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserPost() {
        return userPost;
    }

    public void setUserPost(String userPost) {
        this.userPost = userPost;
    }

    public Object getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(Object userIcon) {
        this.userIcon = userIcon;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserWorkStatus() {
        return userWorkStatus;
    }

    public void setUserWorkStatus(String userWorkStatus) {
        this.userWorkStatus = userWorkStatus;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id='" + id + '\'' +
                ", realName='" + realName + '\'' +
                ", userPost='" + userPost + '\'' +
                ", userIcon=" + userIcon +
                ", organizationId='" + organizationId + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userWorkStatus='" + userWorkStatus + '\'' +
                ", organizationName='" + organizationName + '\'' +
                ", letters='" + letters + '\'' +
                '}';
    }
}