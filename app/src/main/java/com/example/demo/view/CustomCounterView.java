package com.example.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.demo.R;

/**
 * 数量加减控件
 * 两遍加减按钮，中间文本框
 */
public class CustomCounterView extends LinearLayout {
    private Button decreaseButton;   // 减少按钮
    private Button increaseButton;   // 增加按钮
    private TextView valueTextView;  // 显示数值的文本视图

    private int minValue = 0;        // 最小值
    private int maxValue = 100;      // 最大值
    private int increment = 1;       // 增减步进值
    private int value = 0;           // 当前数值
    private OnValueChangedListener valueChangedListener; // 数值变化监听器

    public CustomCounterView(Context context) {
        super(context);
        init(context);
    }

    public CustomCounterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomCounterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.custom_counter_view, this);

        decreaseButton = findViewById(R.id.decrease_button);
        increaseButton = findViewById(R.id.increase_button);
        valueTextView = findViewById(R.id.value_text_view);

        updateValueText();

        decreaseButton.setOnClickListener(v -> {
            decreaseValue();
        });

        increaseButton.setOnClickListener(v -> {
            increaseValue();
        });
    }

    /**
     * 设置数值文本的颜色。
     *
     * @param color 文本颜色
     */
    public void setValueTextColor(int color) {
        valueTextView.setTextColor(color);
    }

    /**
     * 设置数值文本的大小。
     *
     * @param textSize 文本大小（以 sp 为单位）
     */
    public void setValueTextSize(float textSize) {
        valueTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
    }


    /**
     * 设置最小值。
     *
     * @param minValue 最小值
     */
    public void setMinValue(int minValue) {
        this.minValue = minValue;
        if (value < minValue) {
            setValue(minValue);
        }
    }

    /**
     * 设置最大值。
     *
     * @param maxValue 最大值
     */
    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        if (value > maxValue) {
            setValue(maxValue);
        }
    }

    /**
     * 设置增减步进值。
     *
     * @param increment 增减步进值
     */
    public void setIncrement(int increment) {
        this.increment = increment;
    }

    /**
     * 获取当前数值。
     *
     * @return 当前数值
     */
    public int getValue() {
        return value;
    }

    /**
     * 设置数值。
     *
     * @param value 数值
     */
    public void setValue(int value) {
        if (value < minValue) {
            value = minValue;
        }
        if (value > maxValue) {
            value = maxValue;
        }
        this.value = value;
        updateValueText();
        if (valueChangedListener != null) {
            valueChangedListener.onValueChanged(value);
        }
    }

    /**
     * 减少数值。如果当前数值大于最小值，则减少步进值并更新数值文本。
     */
    private void decreaseValue() {
        if (value > minValue) {
            value -= increment;
            updateValueText();
            if (valueChangedListener != null) {
                valueChangedListener.onValueChanged(value);
            }
        }
    }

    /**
     * 增加数值。如果当前数值小于最大值，则增加步进值并更新数值文本。
     */
    private void increaseValue() {
        if (value < maxValue) {
            value += increment;
            updateValueText();
            if (valueChangedListener != null) {
                valueChangedListener.onValueChanged(value);
            }
        }
    }

    /**
     * 更新显示数值的文本视图。
     */
    private void updateValueText() {
        valueTextView.setText(String.valueOf(value));
    }

    public void setOnValueChangedListener(OnValueChangedListener listener) {
        this.valueChangedListener = listener;
    }

    public interface OnValueChangedListener {
        /**
         * 当数值发生变化时调用。
         *
         * @param newValue 新的数值
         */
        void onValueChanged(int newValue);
    }
}