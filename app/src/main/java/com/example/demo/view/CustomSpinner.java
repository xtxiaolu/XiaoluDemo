package com.example.demo.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.example.demo.R;

import java.util.Collection;
import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/7/7
 * 描述: 自定义公共 下拉选择控件
 */
public class CustomSpinner<T> extends LinearLayout {
    private TextView textView;
    private ImageView imageView;
    private PopupWindow popupWindow;
    private List<T> dataList;
    private boolean isExpanded = false;
    private OnItemSelectedListener itemSelectedListener;

    public CustomSpinner(Context context) {
        super(context);
        init();
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout, this, true);
        textView = findViewById(R.id.ll_list_default_txt);
        imageView = findViewById(R.id.ll_list_default_icon);

        setClickable(true);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExpanded) {
                    collapse();
                } else {
                    expand();
                }
            }
        });
    }

    public void setData(List<T> dataList) {
        this.dataList = dataList;
    }

    /**
     * 刷新页面
     *
     * @param data
     */
    public void replaceData(@NonNull Collection<? extends T> data) {
        // 不是同一个引用才清空列表
        if (data != dataList) {
            dataList.clear();
            dataList.addAll(data);
        }
    }

    public void setTextViewValue(String value) {
        textView.setText(value);
    }

    public TextView getTextViewValue() {
        return textView;
    }

    private void expand() {
        if (dataList == null || dataList.isEmpty()) {
            return;
        }

        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.text_order_black));
        imageView.setImageResource(R.drawable.expand_arrows_fold);

        View popupView = LayoutInflater.from(getContext()).inflate(R.layout.popup_selector, null);
        RecyclerView recyclerView = popupView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        CustomAdapter popAdapter = new CustomAdapter<>(R.layout.item_listview_popwin, dataList);
        popAdapter.setSelectedPosition(0);
        popAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                T selectedItem = dataList.get(position);
                if (itemSelectedListener != null) {
                    itemSelectedListener.onItemSelected(position, selectedItem);
                }
                popupWindow.dismiss();
            }

            @Override
            public void convertView(BaseViewHolder holder, Object item, boolean isSelected) {
                if (itemSelectedListener != null) {
                    itemSelectedListener.onItemCallBackData(holder, item);
                }
            }
        });
        recyclerView.setAdapter(popAdapter);

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        popupWindow = new PopupWindow(popupView, width, height);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                collapse();
            }
        });

        int[] location = new int[2];
        getLocationOnScreen(location);
        int x = location[0];
        int y = location[1] + getHeight();
        popupWindow.showAtLocation(this, Gravity.NO_GRAVITY, x, y);

        isExpanded = true;
    }

    private void collapse() {
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorTextBlue));
        imageView.setImageResource(R.drawable.expand_arrows_unfold);

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }

        isExpanded = false;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        this.itemSelectedListener = listener;
    }

    public interface OnItemSelectedListener {
        void onItemSelected(int position, Object item);
        void onItemCallBackData(BaseViewHolder holder, Object item);
    }
}
