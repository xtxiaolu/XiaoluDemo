package com.example.demo.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.demo.R;

/**
 * Created by xiaolu on 2023/8/17.
 * 封装提示信息的Popupwindow
 */
public class TimedPopupWindow {
    private Context mContext;
    private PopupWindow mPopupWindow;
    private Handler mHandler;
    private TextView tvHint;
    private ConstraintLayout clBackground;
    private int mType = 0;
    private int mDurationMillis = 1000; // 默认显示时长为 1000 毫秒

    public TimedPopupWindow(Context context) {
        mContext = context;
        initPopupWindow();
    }

    private void initPopupWindow() {
        // 创建并初始化 PopupWindow
        View popupView = LayoutInflater.from(mContext).inflate(R.layout.popup_layout_timed, null);

        tvHint = popupView.findViewById(R.id.tvHint);
        clBackground = popupView.findViewById(R.id.clBackground);
        mPopupWindow = new PopupWindow(mContext);
        mPopupWindow.setContentView(popupView);
        mPopupWindow.setOutsideTouchable(false);
        mPopupWindow.setFocusable(false);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        // 初始化 Handler
        mHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * 提示类型 根据类型来显示对应的图片
     * @param type 类型 0:默认 1:正确 2:错误
     * @return
     */
    public TimedPopupWindow setType(int type) {
        mType = type;
        if (mType == 1) {
            clBackground.setBackground(mContext.getDrawable(R.drawable.ic_success));
        } else if (mType == 2) {
            clBackground.setBackground(mContext.getDrawable(R.drawable.ic_fail));
        } else {
            clBackground.setBackground(mContext.getDrawable(R.drawable.ic_moren));
        }
        return this;
    }

    /**
     * 更新提示内容
     * @param content 内容
     * @return
     */
    public TimedPopupWindow updateContent(String content) {
        if (tvHint != null) {
            tvHint.setText(content);
        }
        return this;
    }

    /**
     * 设置显示时长
     * @param durationMillis 已毫秒为单位
     * @return
     */
    public TimedPopupWindow setDuration(int durationMillis) {
        mDurationMillis = durationMillis;
        return this;
    }

    public void show() {
        // 检查当前的Activity是否已经销毁
        if (isActivityValid((Activity) mContext)) {
            mPopupWindow.showAtLocation(((Activity) mContext).getWindow().getDecorView(), Gravity.BOTTOM, 0, 225);

            // 在一定时间后自动关闭 PopupWindow
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss(); // 只需要调用 dismiss()，不需要移除回调
                }
            }, mDurationMillis);
        }
    }

    public void dismiss() {
        if (mPopupWindow.isShowing()) {
            if (mHandler != null) {
                mHandler.removeCallbacksAndMessages(null);
            }
            mPopupWindow.dismiss();
        }
    }

    // 检查Activity是否有效
    private boolean isActivityValid(Activity activity) {
        return activity != null && !activity.isFinishing() && !activity.isDestroyed();
    }
}
