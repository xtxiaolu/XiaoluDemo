package com.example.demo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

public class ModifyJsonExample {

    @Test
    public void parseJsonFile() {
        // 从测试资源目录加载 JSON 文件
        InputStream inputStream = getClass().getResourceAsStream("/test_data.json");
        if (inputStream != null) {
            try {
                // 使用 Gson 解析 JSON 数据
                JsonParser jsonParser = new JsonParser();
                JsonObject jsonObject = jsonParser.parse(new InputStreamReader(inputStream)).getAsJsonObject();

                // 获取"data"字段对应的 JSON 数组
                JsonArray dataArray = jsonObject.getAsJsonArray("data");
                // 要修改的组织名称
                String targetOrganizationName = "王家塔厂部"; // 你要修改的组织名称
                // 新的组织名称
                String newOrganizationName = "总监"; // 你要设置的新名称
                // 循环遍历 JSON 数组中的每个对象
                for (int i = 0; i < dataArray.size(); i++) {
                    JsonObject userObject = dataArray.get(i).getAsJsonObject();

                    // 获取"userPhone"字段的值
                    JsonElement userPhoneElement = userObject.get("userPhone");
                    if (userPhoneElement != null && !userPhoneElement.isJsonNull()) {
                        String userPhone = userPhoneElement.getAsString();
                        // 修改"userPhone"字段的值
                        if (userPhone.length() >= 4) {
                            // 生成要替换的字符串，例如用 "X" 替换中间4到末尾的字符
                            String replacement = "X".repeat(userPhone.length()+1 - 4);
                            userPhone = userPhone.substring(0, 3) + replacement;
                            userObject.addProperty("userPhone", userPhone);
                        }
                    }


                    // 获取"organizationName"字段的值
                    String organizationName = userObject.get("organizationName").getAsString();
                    // 检查是否与目标组织名称匹配
                    if (organizationName.equals(targetOrganizationName)) {
                        // 修改"organizationName"字段的值为新的组织名称
                        userObject.addProperty("organizationName", newOrganizationName);
                    }


                    // 获取"organizationName"字段的值
//                    String realName = userObject.get("realName").getAsString();
                    userObject.addProperty("realName", generateRandomName());
                    userObject.addProperty("id", generateRandomNumber());
                    userObject.addProperty("organizationId", generateRandomNumber());
                }

                // 将JsonObject转换回JSON字符串
                String modifiedJsonStr = jsonObject.toString();

                // 打印修改后的JSON字符串
                System.out.println(modifiedJsonStr);
                // 关闭 InputStream
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // 处理文件加载失败的情况
            System.err.println("JSON file not found.");
        }
    }


    // 生成随机的三个字的名字
    public static String generateRandomName() {
        // 姓氏列表
        String[] surnames = {
                "赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈",
                "褚", "卫", "蒋", "沈", "韩", "杨", "朱", "秦", "尤", "许",
                "何", "吕", "施", "张", "孔", "曹", "严", "华", "金", "魏",
                "陶", "姜", "戚", "谢", "邹", "喻", "柏", "水", "窦", "章",
                "云", "苏", "潘", "葛", "奚", "范", "彭", "郎", "鲁", "韦",
                "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳",
                "酆", "鲍", "史", "唐", "费", "廉", "岑", "薛", "雷", "贺",
                "倪", "汤", "滕", "殷", "罗", "毕", "郝", "邬", "安", "常",
                "乐", "于", "时", "傅", "皮", "卞", "齐", "康", "伍", "余",
                "元", "卜", "顾", "孟", "平", "黄", "和", "穆", "萧", "尹"
        };

        // 名字列表
        String[] givenNames = {
                "文", "明", "良", "强", "华", "志", "新", "杰", "博", "俊",
                "春", "秋", "冬", "夏", "雷", "雨", "风", "霜", "雪", "花",
                "平", "和", "康", "宝", "贵", "福", "顺", "安", "祥", "禄",
                "寿", "生", "盛", "富", "财", "金", "玉", "银", "珠", "翠",
                "琳", "慧", "美", "娟", "峰", "云", "帆", "琪", "露", "倩"
        };

        Random random = new Random();

        // 随机选择一个姓氏
        String surname = surnames[random.nextInt(surnames.length)];

        // 随机选择两个名字
        String givenName1 = givenNames[random.nextInt(givenNames.length)];
        String givenName2 = givenNames[random.nextInt(givenNames.length)];

        // 组合成完整的名字
        String fullName = surname + givenName1 + givenName2;

        return fullName;
    }


    // 生成 32 位随机数
    public static String generateRandomNumber() {
        StringBuilder sb = new StringBuilder(32);
        Random random = new Random();

        for (int i = 0; i < 32; i++) {
            // 随机生成一个数字或字母
            char c = (char) ('0' + random.nextInt(36));
            if (c >= '0' && c <= '9') {
                sb.append(c);
            } else {
                sb.append((char) ('a' + random.nextInt(26)));
            }
        }

        return sb.toString();
    }
}
