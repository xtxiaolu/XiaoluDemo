### 项目介绍
Xiaolu demo练习 数据格式基本都是在现实项目中提取修改的仅供参考
仅供个人练习使用
写一些能够让初级做Android的可以做个参考的demo，如果感觉喜欢就收藏，持续更新中！项目中常用的功能！
项目中会写一些，java和kotlin一样的案例，我也刚开始接触kotlin，仅供参考希望不要嫌弃！开干吧！

## 说明
* 规范修改版本递增: 0.0.1
* 模块名: 组件模块化模块名称
* 业务名: 业务功能名称

### 组件包结构
```
com.example.demo
            └── 模块名
                   ├──ui  //业务包
                   │    ├── xxxActivity.java
                   ├──base  //基类
                   │    ├── BaseActivity.java
                   ├──model  //模型
                   │    ├── BaseBean.java
                   ├──data     //数据交互
                   │    ├── DataText.java
                   ├──util    //工具类
                   │    ├── IntentUtil.java
                   ├──view    //自定义view
                   │    ├── TextView.java


mylibrary
com.example.library
                └── library包 存放公共的业务类和工具类
 ```

## 多级列表頁面



|控件实现                          |  实现类                         |  是否不限层级      |  功能简介                      |
|--------------------------------|--------------------------------|------------------|-------------------------------|
| RecyclerView                   |   MultiLevelTreeActivity.java  |  ✔               | 自定义封装控件实现比较容易扩展     |
| BaseRecyclerViewAdapterHelper  |   FixedLeveTreeActivity.java   |  ✘               | 使用常用控件实现写固定层级比较丝滑  |
| ExpandableListView             |   ExpandableTreeActivity.java  |  ✘               | 支持二级列表丝滑，三级列表嵌套实现  |



## library工具包
**Library工具包 简介**

Library工具包收录一系列常用的工具类，包括字符串工具、时间工具、网络工具、日志工具等。该工具包的目标是帮助开发者更轻松、高效地进行Android应用程序的开发，减少重复性代码的编写，提高开发效率。

**主要功能和特点：**

1. **字符串工具类：** 提供了一系列字符串处理方法，包括判空、判空白、字符串拼接、去除空格等，方便开发者对字符串进行操作和处理。

2. **时间工具类：** 封装了日期格式化、日期解析、获取当前时间、计算时间差等功能，帮助开发者轻松处理与时间相关的操作。

3. **网络工具类：** 封装了常见的网络请求方法，支持GET和POST请求，提供异步请求和回调机制，方便开发者实现网络数据的获取和传输。

4. **日志工具类：** 集成了日志记录功能，开发者可以在应用程序中输出调试信息和错误信息，便于调试和错误排查。

5. **其他常用工具类：** 还收录了其他常用的工具类，如文件操作、设备信息获取等，为开发者提供全面的开发支持。

### 组件包结构
```
com.example.library
                └── 模块名
                      ├──tree  // 树结构
                      │    ├── BaseAdapter.java
                      │    ├── BaseModel.java
                      │    ├── BaseTreeAdapter.java
                      ├──utils  // 工具类
                      │    ├── DateUtil.java
                      │    ├── TimeUtil.java
```
    