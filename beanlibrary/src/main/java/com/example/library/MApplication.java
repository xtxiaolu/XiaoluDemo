package com.example.library;

import android.app.Application;
import android.content.Context;

import com.example.library.utils.SpUtil;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.BuildConfig;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;


public class MApplication extends Application {

    private static MApplication instance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        instance = this;

        initLog();
        //初始化sp
        SpUtil.initContext(this);
    }

    private void initLog() {
        /**
         * showThreadInfo   （可选）是否显示线程信息。 默认值为true
         * methodCount(3)   （可选）要显示的方法行数。 默认2
         * methodOffset(0)  （可选）设置调用堆栈的函数偏移值，0的话则从打印该Log的函数开始输出堆栈信息，默认是0
         * tag("Logger")    （可选）TAG内容. 默认是 PRETTY_LOGGER
         */
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(BuildConfig.DEBUG)
                .methodCount(3)
                .methodOffset(0)
                .build();

        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
        Logger.addLogAdapter(new DiskLogAdapter());

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static MApplication getInstance() {
        return instance;
    }
}
