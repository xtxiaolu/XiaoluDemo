package com.example.library.tree;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述: 多级列表  Base 所有多级列表都要继承
 */
public class BaseModel<E extends BaseModel> {
    /**
     * 是否展开
     */
    private boolean isOpen;
    /**
     * 是否选择
     */
    private boolean isCheck;
    /**
     * 上一级父id
     */
    private String pId;
    /**
     * 自己的id
     */
    private String id;

    /**
     * 标记第几级
     */
    private int leave;

    private List<E>children;

    public int getLeave() {
        return leave;
    }

    public void setLeave(int leave) {
        this.leave = leave;
    }

    public List<E> getChildren() {
        return children;
    }

    public void setChildren(List<E> children) {
        this.children = children;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getParentId() {
        return pId;
    }

    public void setParentId(String parentId) {
        this.pId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
