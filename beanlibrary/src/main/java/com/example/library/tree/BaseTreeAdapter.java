package com.example.library.tree;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述: 多级列表  Base 所有多级列表都要继承
 */
public abstract class BaseTreeAdapter<T extends RecyclerView.ViewHolder,E extends BaseModel> extends BaseAdapter<T ,E> {
    public BaseTreeAdapter(Context context, List<E> list) {
        super(context, list);
    }
    private int oldCheck = -1;

    public<W extends BaseModel> void setOpenOrClose(List<W> mList, int pos) {
        W model = mList.get(pos);
        if (model.isOpen()){  //如果是展开  把他关闭
            model.setOpen(false);
            removeChild(model.getId(),mList,0);  //移除子集
        }else {  //关闭状态  就是展开
            model.setOpen(true);
            List<W> children = (List<W>) model.getChildren();
            int size = children.size();
            //pos是你点击的item的position
            int leave = model.getLeave() + 1;
            for (int i = 0;i < size;i++){
                children.get(i).setLeave(leave);
            }
            mList.addAll(pos+1,children);
        }
    }

    private<W extends BaseModel> void removeChild(String parentId, List<W>mList, int start){
        for (int removeIndex = start;removeIndex < mList.size();removeIndex++){
            W model = mList.get(removeIndex);
            if (parentId.equals(model.getParentId())){
                mList.remove(removeIndex);
                removeIndex--;
                //这里使用递归去删除子集的子集
                if (model.getChildren() != null && model.getChildren().size() > 0 && model.isOpen()){
                    model.setOpen(false);
                    removeChild(model.getId(),mList,removeIndex);
                }
            }
        }
    }
}
