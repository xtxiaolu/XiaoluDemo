package com.example.library.tree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * @author: xtxiaolu
 * @date: 2023/6/7
 * 描述: 多级列表  Base 所有多级列表都要继承
 */
public abstract class BaseAdapter<T extends RecyclerView.ViewHolder,E> extends RecyclerView.Adapter<T> {

    protected List<E> list;
    protected Context mContext;
    private OnBaseItemClickListener onBaseItemClickListener;

    public BaseAdapter(Context context, List<E> list) {
        this.list = list;
        this.mContext = context;
    }

    @Override
    public final void onBindViewHolder(@NonNull T holder, int position) {
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onBaseItemClickListener != null){
                    onBaseItemClickListener.onItemClick(view, (int) view.getTag());
                }
            }
        });
        E bean = list.get(position);
        onBindHolder(holder,position,bean);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }
    protected View getLayoutView(int layout){
        return LayoutInflater.from(mContext).inflate(layout,null,false);
    }
    protected View getLayoutView(int layout, ViewGroup parent){
        return LayoutInflater.from(mContext).inflate(layout,parent,false);
    }
    public abstract void onBindHolder(@NonNull T holder,int position,E bean);

    public interface OnBaseItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnBaseItemClickListener(OnBaseItemClickListener onBaseItemClickListener) {
        this.onBaseItemClickListener = onBaseItemClickListener;
    }
}
